from django.contrib import admin

from .models import Award, CourseName, Course, CourseModules, CourseSemesterSetup


@admin.register(Award)
class AwardAdmin(admin.ModelAdmin):
    list_display = ("name", "registration_fee", "module_fee", "exam_fee", )


@admin.register(CourseName)
class CourseNameAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = ("award", "course_name", "duration", "semesters",)


@admin.register(CourseModules)
class CourseModulesAdmin(admin.ModelAdmin):
    list_display = ("course", "module", "compulsory", "semester_no",)


@admin.register(CourseSemesterSetup)
class CourseSetupAdmin(admin.ModelAdmin):
    list_display = ("course", "semester_no", "no_of_modules", "fee",)
