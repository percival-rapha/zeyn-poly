from django.db import models


class EventManager(models.Manager):

    def get_featured_event(self):
        if self.get_queryset().all().count() > 0:
            return self.get_queryset().all()[0]
        return None
