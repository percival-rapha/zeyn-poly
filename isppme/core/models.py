import uuid

from django.db import models

from model_utils.models import TimeStampedModel

class UUIDModel(TimeStampedModel):

    uuid_id = models.UUIDField(
        editable=False, unique=True, default=uuid.uuid4,
    )

    class Meta:
        abstract = True
