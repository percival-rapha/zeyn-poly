from django.contrib import admin

from markdownx.admin import MarkdownxModelAdmin

from .models import DownloadCategory, Download


@admin.register(DownloadCategory)
class DLCAdmin(admin.ModelAdmin):
    list_display = ["name", "created", ]


@admin.register(Download)
class DLAdmin(MarkdownxModelAdmin):
    list_display = ["category", "name", "file", "created", ]
