from django.utils.translation import ugettext_lazy as _

import django_filters

from isppme.courses.models import CourseName


class CourseFilter(django_filters.FilterSet):
    """
    Assignment filter for tutor Portal
    """

    name = django_filters.CharFilter(
        label=_(u"Search course"), lookup_expr='icontains',
    )

    class Meta:
        model = CourseName
        fields = [
            "name",
        ]
