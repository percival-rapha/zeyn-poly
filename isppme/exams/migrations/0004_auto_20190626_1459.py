# Generated by Django 2.2.1 on 2019-06-26 12:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('exams', '0003_auto_20190610_1948'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registrationexam',
            name='exam',
            field=models.ForeignKey(editable=False, help_text='Exam reference', on_delete=django.db.models.deletion.CASCADE, related_name='students', to='exams.Exam', verbose_name='Exam'),
        ),
        migrations.AlterField(
            model_name='registrationexam',
            name='registration',
            field=models.ForeignKey(editable=False, help_text='Student registration reference', on_delete=django.db.models.deletion.CASCADE, related_name='exams', to='enrollments.Registration', verbose_name='Registration'),
        ),
    ]
