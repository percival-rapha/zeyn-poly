# Generated by Django 2.2.1 on 2019-06-02 18:13

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields
import upload_validator
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('classroom', '0003_assignmentdocument_submission'),
    ]

    operations = [
        migrations.CreateModel(
            name='SubmissionDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('uuid_id', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                ('label', models.CharField(blank=True, help_text='Document label. If applicable', max_length=255, null=True, verbose_name='Label')),
                ('file', models.FileField(upload_to='submissions/%Y/%m/', validators=[upload_validator.FileTypeValidator(allowed_types=['image/jpeg', 'text/plain', 'image/png', 'application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'])])),
                ('submission', models.ForeignKey(help_text='Submission ref.', on_delete=django.db.models.deletion.CASCADE, related_name='documents', to='classroom.Submission', verbose_name='Submission')),
            ],
            options={
                'verbose_name': 'Assignment submission document',
                'verbose_name_plural': 'Assignment submission documents',
                'ordering': ('-created', '-modified'),
            },
        ),
    ]
