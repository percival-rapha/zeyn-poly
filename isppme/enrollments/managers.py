from django.db.models import Manager
from django.db import transaction, IntegrityError

from isppme.accounts.models import DebitLedger


class RegistrationManager(Manager):
    """Custom student registration model manager
    """

    use_in_migrations = True
    #
    # def create_registration_record(self, enrollment, semester_no):
    #     course_max_semesters = enrollment.course.semesters
    #     semesters_completed = enrollment.semesters_completed
    #     if semesters_completed >= course_max_semesters:
    #         return
    #     semester_number = semesters_completed + 1
    #
    #     try:
    #         with transaction.atomic():
    #             registration_record = self.model(
    #                 semester=semester,
    #                 semester_no=semester_number,
    #                 enrollment=enrollment,
    #             )
    #         registration_record.save(using=self._db)
    #         self.populate_fees(registration_record)
    #     except IntegrityError:
    #         return None
    #     else:
    #         return registration_record

    @staticmethod
    def populate_fees(registration):

        award = registration.enrollment.course.award
        course = registration.enrollment.course

        # 1. Registration Fees
        reg_entry = DebitLedger(
            student=registration.enrollment.student,
            narration=f"{str(award.name)} Registration Fee",
            amount=award.registration_fee,
        )
        reg_entry.save()

        # 2. Tuition/School Fee(s)
        fee_data = course.fees.filter(
            semester_no=registration.semester_no,
        )
        if fee_data.count() > 0:
            fees_record = fee_data[0]

            tuition_entry = DebitLedger(
                student=registration.enrollment.student,
                narration=f"{str(fees_record.__str__())}",
                amount=fees_record.fee,
            )
