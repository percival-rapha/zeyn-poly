from django.shortcuts import get_object_or_404, render, reverse
from django.views import generic, View
from django.utils.text import slugify
from django.contrib.auth import views as auth_views, logout, get_user_model
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseServerError

from django_hosts.resolvers import reverse as hosts_reverse, reverse_host_lazy as hosts_reverse_lazy
from django_filters.views import FilterView
from django_chatter.utils import create_room
from paynow import Paynow

from isppme.enrollments.models import Enrollment, Registration, RegisteredModules
from isppme.semesters.models import Semester
from isppme.accounts.models import DebitLedger, CreditLedger, Transaction
from isppme.classroom.models import Assignment, Submission, RegistrationAssignments, Discussion
from isppme.modules.models import Module
from isppme.exams.models import Exam, RegistrationExam

from .forms import (
    StudentAuthForm, ModuleChooser, SubmissionForm, SubmissionDocumentForm,
    DiscussionForm, DiscussionCommentForm, CustomPasswordChangeForm
)
from .filters import AssignmentFilter
from .mixins import StudentLoginRequired, StudentContextMixin, HasZeroBalance


class BaseStudentView(StudentLoginRequired, StudentContextMixin):
    pass


class Login(auth_views.LoginView):
    template_name = "students/login.html"
    authentication_form = StudentAuthForm

    def get_success_url(self):
        return hosts_reverse("dashboard", host="students", )


login_view = Login.as_view()


class Logout(StudentLoginRequired, View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(redirect_to=self.get_login_url())


logout_view = Logout.as_view()


class Dashboard(StudentLoginRequired, StudentContextMixin, generic.TemplateView):
    template_name = "students/dashboard.html"


dashboard_view = Dashboard.as_view()


class EnrollmentOverView(StudentLoginRequired, StudentContextMixin, generic.DetailView):
    template_name = "students/enrollment_overview.html"
    model = Enrollment
    context_object_name = "enrollment"
    slug_field = 'uuid_id'
    slug_url_kwarg = "enrollment_uuid"

    # TODO: Restrict access if student is enrollment object owner


enrollment_overview_view = EnrollmentOverView.as_view()


class TranscriptView(BaseStudentView, generic.DetailView):
    template_name = "students/transcript.html"
    model = Enrollment
    context_object_name = "enrollment"
    slug_field = 'uuid_id'
    slug_url_kwarg = "enrollment_uuid"


transcript_view = TranscriptView.as_view()


class RegisterView(StudentLoginRequired, UserPassesTestMixin, StudentContextMixin, generic.TemplateView, View):
    template_name = "students/registration_index.html"

    def test_func(self):
        CURRENT_SEMESTER = Semester.objects.get_current_semester()
        return self.enrollment.can_register and CURRENT_SEMESTER

    def dispatch(self, request, *args, **kwargs):
        CURRENT_SEMESTER = Semester.objects.get_current_semester()
        self.enrollment = get_object_or_404(Enrollment, uuid_id=kwargs["enrollment_uuid"])
        self.semester = CURRENT_SEMESTER
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["enrollment"] = self.enrollment
        return ctx

    def post(self, request, *args, **kwargs):
        if "register" in self.request.POST:
            if self.semester:
                self.semester.register_enrollment(self.enrollment)

        return HttpResponseRedirect(
            redirect_to=hosts_reverse("enrollment_overview", kwargs={"enrollment_uuid": self.enrollment.uuid_id},
                                      host="students")
        )


registration_index = RegisterView.as_view()


class SelectModules(StudentLoginRequired, StudentContextMixin, UserPassesTestMixin, generic.FormView):
    template_name = "students/select_modules.html"
    form_class = ModuleChooser

    def test_func(self):
        return self.request.user.student == self.record.enrollment.student and not self.record.modules_confirmed

    def get_success_url(self):
        return hosts_reverse("enrollment_overview", kwargs={"enrollment_uuid": self.record.enrollment.uuid_id},
                             host="students")

    def dispatch(self, request, *args, **kwargs):
        self.record = get_object_or_404(Registration, uuid_id=kwargs["registration_uuid"])
        return super().dispatch(request, *args, **kwargs)

    def get_form(self, form_class=None):
        from .forms import ModuleChooser
        fm = ModuleChooser(**self.get_form_kwargs())
        fm.fields["choose_modules"].queryset = self.record.get_optional_modules()
        return fm

    def form_valid(self, form):
        # TODO: add message
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["record"] = self.record
        return ctx

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            modules = form.cleaned_data.get("choose_modules", None)
            if not modules:
                form.add_error(None, "Please choose modules")
                return self.form_invalid(form)

            selected = list()
            for md in modules:
                selected.append(md)
            additional_modules_needed = self.record.required_modules - self.record.get_compulsory_modules().count()
            if self.record.get_compulsory_modules().count() + len(selected) < self.record.required_modules:
                form.add_error(None, f"Please select at least {additional_modules_needed} more modules.")
                return self.form_invalid(form)

            self.record.apply_module_choices(choices=selected)
            return self.form_valid(form)
        else:
            return self.form_invalid(form)



            # def post(self, request, *args, **kwargs):
            #     from .forms import ModuleChooser
            #     mc = ModuleChooser(request.POST)
            #     mc.fields["choose_modules"].queryset = self.get_object().get_optional_modules()
            #     if mc.is_valid():
            #         ss = ""
            #         for md in mc.cleaned_data.get("selected_modules"):
            #             ss += md.module.code
            #
            #
            #         return HttpResponse(ss)


select_modules_view = SelectModules.as_view()


class PreparePayment(StudentLoginRequired, StudentContextMixin, UserPassesTestMixin, generic.DetailView):
    model = DebitLedger
    template_name = "students/prepare_payment.html"
    context_object_name = "fee"
    slug_field = "uuid_id"
    slug_url_kwarg = "entry_uuid"

    def test_func(self):
        return self.get_object().registration.modules_confirmed

    def post(self, request, *args, **kwargs):
        fee = self.get_object()
        if fee.merchant:
            return_url = f"http://students.isppme.com/dashboard"
            result_url = f"http://students.isppme.com/dashboard"
            paynow = Paynow(
                fee.merchant.integration_id,
                fee.merchant.integration_key,
                return_url=return_url,
                result_url=result_url,
            )

            # Create payment ref
            payment = paynow.create_payment("ISPPME Students Fees")
            del payment.items[:]
            payment.add(fee.narration, fee.amount)
            # Save the response from paynow in a variable
            try:
                response = paynow.send(payment)
            except Exception as ex:
                # TODO: log message and send to user
                print(str(ex))
                return HttpResponseServerError("Failed to initiate transaction. Please try again in a few minutes")
            else:
                if response.success:
                    # URL from Paynow to poll for status
                    poll_url = response.poll_url

                    # Payment URL to redirect user to
                    link = response.redirect_url

                    # Create paynow transaction in db
                    trans = Transaction.objects.create(
                        poll_url=poll_url, link=link, debit_ref=fee,
                    )

                    context = {
                        "transaction": trans,
                        "proceed": hosts_reverse(
                            "enrollment_overview",
                            kwargs={"enrollment_uuid": fee.registration.enrollment.uuid_id},
                            host="students"
                        ),
                    }

                    return HttpResponse(
                        render(request, "students/payment_status.html", context=context),
                    )

        else:
            return HttpResponseServerError("Could not start transaction. Please try again later")


prepare_payment_view = PreparePayment.as_view()


class ModulesListView(StudentLoginRequired, HasZeroBalance, StudentContextMixin, generic.ListView):
    """
    Lists all modules studied by student in current semester.
    """
    template_name = "students/module_list.html"
    context_object_name = "modules"

    def get_queryset(self):
        CURRENT_SEMESTER = Semester.objects.get_current_semester()
        return RegisteredModules.objects.filter(
            registration_record__semester=CURRENT_SEMESTER,
            registration_record__enrollment__student=self.request.user.student
        )


modules_list_view = ModulesListView.as_view()


class ModuleDetails(StudentLoginRequired, HasZeroBalance, StudentContextMixin, generic.DetailView):
    model = Module
    template_name = "students/module_details.html"
    context_object_name = "module"
    slug_url_kwarg = "module_uuid"
    slug_field = "uuid_id"


module_detail_view = ModuleDetails.as_view()


class ModuleAssignments(StudentLoginRequired, HasZeroBalance, StudentContextMixin, generic.ListView):
    """
    Lists all assignments for current semester in a module.
    """

    template_name = "students/module_assignment_list.html"
    context_object_name = "assignments"

    def __init__(self, **kwargs):
        self.module_obj = None
        super(ModuleAssignments, self).__init__(**kwargs)

    def get_queryset(self):
        self.module_obj = get_object_or_404(Module, uuid_id=self.kwargs["module_uuid"])
        return self.module_obj.current_semester_assignments()

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["module"] = self.module_obj
        return ctx


module_assignment_list = ModuleAssignments.as_view()


class AssignmentsView(StudentLoginRequired, HasZeroBalance, StudentContextMixin, FilterView):
    """Gets all assignments in a particular enrollment object"""
    template_name = "students/enrollment_assignment_list.html"
    filterset_class = AssignmentFilter
    context_object_name = "assignments"

    def get_queryset(self):
        # Returns assignments for student in current semester
        CURRENT_SEMESTER = Semester.objects.get_current_semester()
        return RegistrationAssignments.objects.filter(
            registration__semester=CURRENT_SEMESTER, registration__enrollment__student=self.request.user.student,
        )

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


enrollment_assignments_view = AssignmentsView.as_view()


class SubmitAssignment(StudentLoginRequired, HasZeroBalance, StudentContextMixin, generic.CreateView):
    """Creates an assignment submission
    entry/record.
    """

    template_name = "students/submit_assignment.html"
    form_class = SubmissionForm

    def dispatch(self, request, *args, **kwargs):
        self.assignment = get_object_or_404(Assignment, uuid_id=kwargs['assignment_uuid'])
        self.student = self.request.user.student
        self.initial.update(
            {
                "assignment": self.assignment,
                "student": self.student,
            }
        )
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return hosts_reverse('submission_details', kwargs={"submission_uuid": self.object.uuid_id}, host="students")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["assignment"] = self.assignment
        return context


submit_assignment_view = SubmitAssignment.as_view()


class SubmissionView(StudentLoginRequired, HasZeroBalance, StudentContextMixin, generic.DetailView):
    """Displays a submission made by student
    """

    template_name = "students/submission_details.html"
    model = Submission
    context_object_name = "submission"
    slug_field = "uuid_id"
    slug_url_kwarg = "submission_uuid"


submission_details_view = SubmissionView.as_view()


class EditSubmissionView(StudentLoginRequired, HasZeroBalance, StudentContextMixin, generic.UpdateView):
    form_class = SubmissionForm
    model = Submission
    template_name = "students/edit_submission.html"
    context_object_name = "submission"
    slug_field = "uuid_id"
    slug_url_kwarg = "submission_uuid"

    def get_success_url(self):
        messages.success(self.request, "Assignment submission successfully updated.")
        return hosts_reverse('submission_details', kwargs={"submission_uuid": self.object.uuid_id}, host="students")


edit_submission_view = EditSubmissionView.as_view()


class AttachSubmissionDocuments(StudentLoginRequired, HasZeroBalance, StudentContextMixin, generic.CreateView):
    """Attaches document(s) to an Assignment
    """
    form_class = SubmissionDocumentForm
    template_name = "students/attach_submission_document.html"

    def __init__(self, **kwargs):
        self.submission = None
        super().__init__(**kwargs)

    def dispatch(self, request, *args, **kwargs):
        self.submission = get_object_or_404(Submission, uuid_id=kwargs['submission_uuid'])
        self.initial.update(
            {
                "submission": self.submission,
            }
        )
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return hosts_reverse('submission_details', kwargs={"submission_uuid": self.submission.uuid_id}, host="students")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["submission"] = self.submission
        return context


attach_submission_document_view = AttachSubmissionDocuments.as_view()


class AssignmentsList(StudentLoginRequired, HasZeroBalance, StudentContextMixin, generic.ListView):
    template_name = "students/assignment_list.html"
    context_object_name = "assignments"

    def get_queryset(self):
        CURRENT_SEMESTER = Semester.objects.get_current_semester()
        return RegistrationAssignments.objects.filter(
            registration__semester=CURRENT_SEMESTER, registration__enrollment__student=self.request.user.student
        )

    def dispatch(self, request, *args, **kwargs):
        student = self.request.user.student
        for enrollment in student.enrollments.all():
            for reg in enrollment.registrations.all():
                Enrollment.add_missing_assignments(reg)
        return super().dispatch(request, *args, **kwargs)


assignments_list_view = AssignmentsList.as_view()


class AssignmentDetails(StudentLoginRequired, HasZeroBalance, StudentContextMixin, generic.DetailView):
    model = Assignment
    context_object_name = "assignment"
    slug_field = "uuid_id"
    slug_url_kwarg = "assignment_uuid"
    template_name = "students/assignment_details.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        submission = self.get_object().submissions.filter(student=self.request.user.student)
        context["submission"] = submission
        return context


assignment_details_view = AssignmentDetails.as_view()


#####################################################################################################
# Exams                                                                                             #
# Exam views for students                                                                           #
# Written by Percival Rapha (c) ISPPME 2019                                                         #
#####################################################################################################

class ExamListing(StudentLoginRequired, HasZeroBalance, StudentContextMixin, generic.ListView):
    template_name = "students/exam_listing.html"
    context_object_name = "exams"

    def get_queryset(self):
        return RegistrationExam.objects.filter(
            registration=self.registration_record, exam__semester=self.current_semester
        )

    def dispatch(self, request, *args, **kwargs):
        student = self.request.user.student
        for enrollment in student.enrollments.all():
            for reg in enrollment.registrations.all():
                Enrollment.add_missing_exams(reg)
        return super().dispatch(request, *args, **kwargs)


exam_listing_view = ExamListing.as_view()


class ExamCalendar(ExamListing):
    template_name = "students/exam_calendar.html"


exam_calendar_view = ExamCalendar.as_view()


#####################################################################################################
# Discussions                                                                                       #
# Discussion views for students                                                                     #
# Written by Percival Rapha (c) ISPPME 2019                                                         #
#####################################################################################################


class Discussions(BaseStudentView, HasZeroBalance, generic.ListView):
    template_name = "students/discussions.html"
    context_object_name = "discussions"

    def get_queryset(self):
        CURRENT_SEMESTER = Semester.objects.get_current_semester()
        modules = [
            m.module for m in RegisteredModules.objects.filter(
                registration_record__semester=CURRENT_SEMESTER,
                registration_record__enrollment__student=self.request.user.student
            )
        ]
        return Discussion.objects.filter(
            module_ref__in=modules,
        )


discussions_view = Discussions.as_view()


class DiscussionDetails(BaseStudentView, HasZeroBalance, generic.DetailView, generic.edit.FormMixin):
    model = Discussion
    slug_url_kwarg = "discussion_uuid"
    slug_field = "uuid_id"
    template_name = "students/discussion_details.html"
    context_object_name = "discussion"

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["comment_form"] = DiscussionCommentForm(initial={"discussion": self.get_object()})
        return ctx

    def get_form(self, form_class=None):
        return DiscussionCommentForm(**self.get_form_kwargs())

    def dispatch(self, request, *args, **kwargs):
        self.initial.update({"discussion": self.get_object()})
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.student = self.request.user.student
        form.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        messages.success(self.request, "Comment successfully posted.")
        return hosts_reverse(
            "discussion",
            kwargs={
                "discussion_uuid": self.get_object().uuid_id,
                "title": slugify(self.get_object().title)
            },
            host="students"
        )

    def post(self, request, *args, **kwargs):
        """
        Handle POST requests: instantiate a form instance with the passed
        POST variables and then check if it's valid.
        """
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


discussion_details = DiscussionDetails.as_view()


#####################################################################################################
# Chat                                                                                              #
# Chat views for students                                                                           #
# Written by Percival Rapha (c) ISPPME 2019                                                         #
#####################################################################################################


class ChatIndex(BaseStudentView, HasZeroBalance, generic.TemplateView):
    template_name = "students/select_chat_buddy.html"

    def get_context_data(self, **kwargs):
        CURRENT_SEMESTER = Semester.objects.get_current_semester()
        ctx = super().get_context_data(**kwargs)
        ctx["modules"] = RegisteredModules.objects.filter(
            registration_record__semester=CURRENT_SEMESTER,
            registration_record__enrollment__student=self.request.user.student
        )
        return ctx


chat_index_view = ChatIndex.as_view()


class GetChatUrl(BaseStudentView, HasZeroBalance, View):
    def get(self, request, *args, **kwargs):
        user = get_user_model().objects.get(username=request.user)
        target_user = get_user_model().objects.get(pk=kwargs["target_user"])

        '''
        AI-------------------------------------------------------------------
            Use the util room creation function to create room for one/two
            user(s). This can be extended in the future to add multiple users
            in a group chat.
        -------------------------------------------------------------------AI
        '''
        if (user == target_user):
            room_id = create_room([user])
        else:
            room_id = create_room([user, target_user])
        return HttpResponseRedirect(
            reverse('django_chatter:chatroom', args=[room_id])
        )


get_chat_url_view = GetChatUrl.as_view()


#####################################################################################################
# Account Settings                                                                                  #
# Account Settings views for students                                                               #
# Written by Percival Rapha (c) ISPPME 2019                                                         #
#####################################################################################################


class ChangePasswordView(BaseStudentView, auth_views.PasswordChangeView):
    template_name = "students/change_password.html"
    form_class = CustomPasswordChangeForm

    def get_success_url(self):
        return hosts_reverse("login", host="students")


change_password_view = ChangePasswordView.as_view()


class CreateDiscussionView(BaseStudentView, HasZeroBalance, generic.CreateView):
    form_class = DiscussionForm
    template_name = "students/create_discussion.html"

    def __init__(self, **kwargs):
        self.module_ref = None
        super().__init__(**kwargs)

    def form_valid(self, form):
        form.instance.created_by = self.request.user.student
        return super().form_valid(form)

    def dispatch(self, request, *args, **kwargs):
        self.module_ref = get_object_or_404(Module, uuid_id=kwargs["module_uuid"])
        self.initial.update(
            {"module_ref": self.module_ref}
        )
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["module"] = self.module_ref
        return ctx

    def get_success_url(self):
        return hosts_reverse("discussion", kwargs={"discussion_uuid": self.object.uuid_id,
                                                   "title": slugify(self.object.title)}, host="students")


create_discussion_view = CreateDiscussionView.as_view()
