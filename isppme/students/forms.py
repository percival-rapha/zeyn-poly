from django import forms
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.utils.translation import ugettext_lazy as _

from crispy_forms.layout import Layout, Field, Fieldset, Div, HTML, ButtonHolder, Submit
from crispy_forms.helper import FormHelper

from isppme.enrollments.models import RegisteredModules
from isppme.classroom.models import Submission, SubmissionDocument, Discussion, DiscussionComment


class StudentAuthForm(AuthenticationForm):
    def confirm_login_allowed(self, user):
        if not user.is_active:
            raise forms.ValidationError(
                _("This account is inactive."),
                code='inactive',
            )
        if not user.is_student:
            raise forms.ValidationError(
                _("Access denied."), code="not_allowed"
            )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-prepended'


class ModuleChooser(forms.Form):
    choose_modules = forms.ModelMultipleChoiceField(
        queryset=None,
        widget=forms.CheckboxSelectMultiple(
            attrs={}
        )
    )

    def clean(self):
        data = super().clean()


class SubmissionForm(forms.ModelForm):
    class Meta:
        model = Submission
        fields = [
            "assignment", "student", "write_up",
        ]
        widgets = {
            "assignment": forms.HiddenInput(),
            "student": forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Submit'))


class SubmissionDocumentForm(forms.ModelForm):
    class Meta:
        model = SubmissionDocument
        fields = [
            "submission", "label", "file",
        ]
        widgets = {
            "submission": forms.HiddenInput()
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Attach'))


class DiscussionForm(forms.ModelForm):
    class Meta:
        model = Discussion
        fields = [
            "title", "module_ref", "details",
        ]
        widgets = {
            "module_ref": forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Post'))

class DiscussionCommentForm(forms.ModelForm):
    class Meta:
        model = DiscussionComment
        fields = ["discussion", "details"]
        widgets = {
            "discussion": forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Post'))


class CustomPasswordChangeForm(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Submit'))
