from django.apps import AppConfig


class ChatConfig(AppConfig):
    name = 'isppme.chat'
