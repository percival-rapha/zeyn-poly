import csv

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.core import serializers
from django.http import HttpResponse

from isppme.semesters.models import Semester

REGISTRATION_SEMESTER = Semester.objects.get_current_semester()

from .models import Enrollment, Registration, RegisteredModules, EnrollmentSemesters


@admin.register(Enrollment)
class EnrollmentAdmin(admin.ModelAdmin):
    list_display = ("student", "course", "semesters_completed", "semesters_remaining", "completed")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if REGISTRATION_SEMESTER:
            self.actions.append('progress')

    def progress(self, request, queryset):
        for enrollment in queryset:
            if REGISTRATION_SEMESTER:
                enrollment.progress_semester(REGISTRATION_SEMESTER)
    progress.short_description = _(f"Progress to semester {REGISTRATION_SEMESTER}")



@admin.register(Registration)
class RegistrationAdmin(admin.ModelAdmin):
    list_display = ("enrollment", "semester_no", "semester", "complete", "modules_confirmed", )
    list_filter = ('complete', 'enrollment__course__course_name__name', 'enrollment__course__award__name', )
    actions = ['export', ]

    def export(self, request, queryset):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="export.csv"'
        writer = csv.writer(response)
        writer.writerow(['#', "REGISTRATION NUMBER", "FULL NAME", ])
        for idx, rec in enumerate(queryset):
            student = rec.enrollment.student
            writer.writerow(
                [idx+1, student.reg_number, student.full_name,]
            )
        return response

@admin.register(RegisteredModules)
class RegisteredModulesAdmin(admin.ModelAdmin):
    list_display = (
    "registration_record", "module", "compulsory", "core", "exam_total", "continuous_assessment", "score",
    "classification", "decision",)
