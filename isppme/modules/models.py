import html

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import ValidationError

from markdownx.models import MarkdownxField
from markdownx.utils import markdownify

from isppme.core.models import UUIDModel
from isppme.semesters.models import Semester

CURRENT_SEMESTER = Semester.objects.get_current_semester()


class Module(UUIDModel):
    """Stores a representation of a module or
    subject.
    e.g HCS 101 Introduction to project planning,
    NC 205 Advanced Project Evaluation etc.
    """

    code = models.CharField(
        verbose_name=_("Module Code"), max_length=20, unique=True,
        help_text=_("Module code e.g. HCS 101, NC 205, etc.")
    )
    name = models.CharField(
        verbose_name=_("Module name"), max_length=255,
        help_text=_("Module name e.g. Introduction to computing.")
    )

    continuous_assessment_contribution = models.IntegerField(
        verbose_name=_("Continuous assessment percentage contribution"), default=30,
        help_text=_("Indicates the percentage contributed by assignments")
    )
    exam_contribution = models.IntegerField(
        verbose_name=_("Exam percentage contribution"), default=70,
        help_text=_("Indicates the exam score contributes to overal module points.")
    )

    class Meta:
        verbose_name = _("Module")
        verbose_name_plural = _("Modules")
        ordering = ("code", "name",)
        unique_together = ("code", "name",)

    def __str__(self):
        return f"{str(self.code)} - {str(self.name)}"

    def clean(self):
        if self.continuous_assessment_contribution and self.exam_contribution:
            if self.continuous_assessment_contribution + self.exam_contribution > 100:
                raise ValidationError(_("Total contribution cannot be greater than 100, please adjust."))

    def save(self, *args, **kwargs):
        self.full_clean()
        super(Module, self).save(*args, **kwargs)

    def current_semester_students(self):
        from isppme.enrollments.models import RegisteredModules
        if CURRENT_SEMESTER:
            return RegisteredModules.objects.filter(
                module=self, registration_record__semester=CURRENT_SEMESTER
            ).count()

    def current_semester_assignments(self):
        if CURRENT_SEMESTER:
            return self.assignments.filter(
                created__range=(CURRENT_SEMESTER.start_datetime, CURRENT_SEMESTER.end_datetime)
            )

    def get_current_semester_students(self):
        from isppme.enrollments.models import RegisteredModules
        if CURRENT_SEMESTER:
            reg_mod = RegisteredModules.objects.filter(
                module=self, registration_record__semester=CURRENT_SEMESTER
            )
            return [r.registration_record.enrollment.student for r in reg_mod]


class LearningMaterial(UUIDModel):
    """Stores learning material per
    module.
    """

    module_ref = models.ForeignKey(
        to="modules.Module", on_delete=models.CASCADE, related_name="learning_material",
        verbose_name=_("Module"), help_text=_("Reference to module")
    )

    label = models.CharField(
        verbose_name=_('Label'), max_length=255,
        help_text=_("Label/Material title")
    )

    file = models.FileField(
        verbose_name=_("File"), null=True, blank=True,
        upload_to="modules/lm/%Y/%m/",
    )

    about = MarkdownxField(
        verbose_name=_("About"), null=True, blank=True
    )

    class Meta:
        verbose_name = _("Learning material")
        verbose_name_plural = _("Learning materials")
        ordering = ("-created",)

    @property
    def get_about_markdownified(self):
        return markdownify(html.escape(self.about))


class MaterialUploads(UUIDModel):
    module_ref = models.ForeignKey(
        to=Module, on_delete=models.CASCADE, related_name="material_uploads",
    )
    name = models.CharField(
        max_length=255,
    )

    file = models.FileField(
        upload_to="modules/material-uploads/%Y/%m/", null=True, blank=True,
    )
    url = models.CharField(
        max_length=255, verbose_name=_("Link"), null=True, blank=True,
    )

    class Meta:
        verbose_name = _("Module material upload")
        verbose_name_plural = _("Module material uploads")
