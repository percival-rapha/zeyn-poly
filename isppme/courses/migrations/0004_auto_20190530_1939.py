# Generated by Django 2.2.1 on 2019-05-30 17:39

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0003_award_late_registration_fee'),
    ]

    operations = [
        migrations.CreateModel(
            name='CourseSemesterSetup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('uuid_id', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                ('semester_no', models.PositiveSmallIntegerField(default=1, help_text='Semester number', validators=[django.core.validators.MinValueValidator(1, message='Semester number must be greater than 0')], verbose_name='Semester no.')),
                ('no_of_modules', models.PositiveSmallIntegerField(default=1, validators=[django.core.validators.MinValueValidator(1, message='Number of modules must be greater than 0')], verbose_name='Number of modules')),
                ('fee', models.DecimalField(decimal_places=2, max_digits=19, verbose_name='Fee')),
                ('course', models.ForeignKey(help_text='Course reference.', on_delete=django.db.models.deletion.CASCADE, related_name='semester_setup', to='courses.Course', verbose_name='Course')),
            ],
            options={
                'verbose_name': 'course setup',
                'verbose_name_plural': 'course setups for semesters',
                'unique_together': {('course', 'semester_no')},
            },
        ),
        migrations.DeleteModel(
            name='CourseFees',
        ),
    ]
