from django.urls import path, include
from django.conf import settings

from .views import check_paynow_transaction_status_view


urlpatterns = [
    path(r'check-paynow-transaction-status/<uuid:transaction_uuid>/', check_paynow_transaction_status_view, name="check_paynow_transaction_status"),
]


if "debug_toolbar" in settings.INSTALLED_APPS and settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
