from django.db.models import Manager
from django.utils import timezone


class SemesterManager(Manager):
    use_in_migrations = True

    def get_current_semester(self):
        try:
            return super().get_queryset().get(
                registration_start__lte=timezone.now().date(), end__gte=timezone.now().date()
            )
        except Exception as ex:
            # TODO: Log error
            return None

    def get_application_semester(self):
        try:
            return super().get_queryset().get(
                applications_start__lte=timezone.now().date(), end__gte=timezone.now().date()
            )
        except Exception as ex:
            # TODO: Log error
            return None

    def get_registering_semester(self):
        semester = None
        try:
            semester = super().get_queryset().get(
                registration_start__lte=timezone.now().date(), end__gte=timezone.now().date()
            )
        except Exception as ex:
            # TODO: log error
            pass

        return semester
