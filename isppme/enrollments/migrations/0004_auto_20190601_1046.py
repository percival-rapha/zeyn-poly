# Generated by Django 2.2.1 on 2019-06-01 08:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('enrollments', '0003_enrollment_deffered'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='registration',
            options={'ordering': ('semester_no',), 'verbose_name': 'Registration record', 'verbose_name_plural': 'Registration records'},
        ),
        migrations.AddField(
            model_name='registration',
            name='modules_confirmed',
            field=models.BooleanField(default=False, editable=False, verbose_name='Modules confirmed'),
            preserve_default=False,
        ),
    ]
