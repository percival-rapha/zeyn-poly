import datetime
from datetime import date

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.validators import ValidationError, RegexValidator

from model_utils import Choices
from upload_validator import FileTypeValidator

from isppme.core.models import UUIDModel
from isppme.core.countries import COUNTRIES
from isppme.courses.models import Course

# TODO: Add note(s) to applicant(s) in text file.


class Application(UUIDModel):
    """Stores applications by guests
    """
    ENTRY_TYPES = (
        ("N", _("Normal")),
        ("S", _("Special")),
        ("M", _("Mature")),
        ("O", _("Occasional")),
        ("R", _("Repeat")),
        ("T", _("Transfer")),
    )
    course = models.ForeignKey(
        Course, on_delete=models.SET_NULL, null=True, blank=True,
        verbose_name=_("Course"), related_name="applications",
        help_text=_("Course being applied for e.g. Diploma - M&E")
    )
    entry_type = models.CharField(
        verbose_name=_("Entry type"), max_length=1, choices=ENTRY_TYPES,
        help_text=_("Please select entry type in accordance to course being applied for.")
    )
    # Set applicants age limit here.
    AGE_LIMIT = 10

    MARITAL_STATUSES = (
        ("M", _("Married")),
        ("S", _("Single")),
        ("D", _("Divorced")),
        ("W", _("Widowed")),
    )
    SEX_CHOICES = (
        ("M", _("Male")),
        ("F", _("Female")),
    )
    MR = 'MR'
    MRS = 'MRS'
    MS = 'MS'
    DR = 'DR'
    MISS = "MISS"
    REV = "REV"
    SR = "SR"
    TITLES = (
        (MR, _("Mr")),
        (MRS, _("Mrs")),
        (MS, _("Ms")),
        (DR, _("Dr")),
        (MISS, _("Miss")),
        (REV, _("Rev")),
        (SR, _("Sr")),
    )
    title = models.CharField(
        verbose_name=_("Title"), max_length=4, choices=TITLES,
        default=MR, help_text=_("Title e.g. Mr/Mrs/Ms etc.")
    )
    first_name = models.CharField(
        verbose_name=_("First name"), max_length=255,
        help_text=_("Applicant's first name(s)"),
    )
    last_name = models.CharField(
        verbose_name=_("Lastname"), max_length=255,
        help_text=_("Applicant's lastname or surname")
    )
    sex = models.CharField(
        verbose_name=_("Sex"), max_length=1, choices=SEX_CHOICES,
        help_text=_("Applicant's sex orientation")
    )
    date_of_birth = models.DateField(
        verbose_name=_("Date of birth"),
        help_text=_("Applicant's date of birth")
    )
    place_of_birth = models.CharField(
        verbose_name=_("Place of birth"), max_length=255,
        help_text=_("Applicant's place of birth")
    )
    marital_status = models.CharField(
        verbose_name=_("Marital status"), max_length=1, choices=MARITAL_STATUSES,
        default="S", help_text=_("Applicant's marital status")
    )
    national_id = models.CharField(
        verbose_name=_("National ID"), null=True, blank=True, max_length=20,
        help_text=_("Applicant's national id number as issued by country of nationality")
    )
    passport_number = models.CharField(
        verbose_name=_("Passport number"), null=True, blank=True, max_length=25,
        help_text=_("Applicant's passport number is applicable")
    )
    nationality = models.CharField(
        choices=COUNTRIES, null=True, blank=True, max_length=255,
        verbose_name=_("Nationality"),
        help_text=_("Applicant's nationality")
    )
    country_of_permanent_residence = models.CharField(
        choices=COUNTRIES, null=True, blank=True, max_length=255,
        verbose_name=_("Country of permanent residence"),
        help_text=_("Applicant's country of permanent residence")
    )
    disabilities = models.TextField(
        verbose_name=_("Disabilities"), max_length=599, blank=True, null=True,
        help_text=_("Disabilities if any")
    )
    contact_address = models.CharField(
        verbose_name=_("Physical Address"), max_length=255,
        help_text=_("Note: All correspondences will be forwarded to this address")
    )
    telephone_number = models.CharField(
        verbose_name=_("Telephone number"), max_length=20, null=True, blank=True,
        help_text=_("Applicant's home telephone number"),
        validators=[
            RegexValidator(
                regex=r'^\+\d+$',
                message="Please enter number with country code in it."
            )
        ]
    )
    mobile_number = models.CharField(
        verbose_name=_("Mobile number"), max_length=20, null=True, blank=True,
        help_text=_("Applicant's mobile phone number"),
        validators=[
            RegexValidator(
                regex=r'^\+\d+$',
                message="Please enter number with country code in it."
            )
        ]
    )
    email_address = models.EmailField(
        verbose_name=_("Email address"), max_length=255,
        help_text=_("Applicant's email address")
    )
    is_member = models.BooleanField(
        verbose_name=_("Are you a Member of ISPPME?"), default=False,
        help_text=_("Indicate if applicant is a member of ISPPME")
    )
    member_id = models.CharField(
        verbose_name=_("Member ID"), blank=True, null=True, max_length=30,
        help_text=_("Enter member id if indicated as a member of ISPPME")
    )
    colleges_attended = models.TextField(
        verbose_name=_("Colleges/Universities Attended"), max_length=500, null=True, blank=True,
        help_text=_("In the following format: Name, Qualifications, Year")
    )
    work_experience = models.TextField(
        verbose_name=_("Work experience/Employment"), max_length=500, null=True, blank=True,
        help_text=_(
            "Indicate period, Occupation And Employer's Address in format: Period, Occupation, Employer Address")
    )
    referees = models.TextField(
        verbose_name=_("Referees"), max_length=500,
        help_text=_("Names and addresses of 2 referees. In format: Name Address, Name Address")
    )
    acknowledged = models.BooleanField(
        verbose_name=_("Acknowledged"), default=False,
        help_text=_("Indicates applicant has acknowledged accpetance and registered.")
    )
    STATUSES = Choices(
        ("pending", _("pending")),
        ("accepted", _("accepted")),
        ("rejected", _("rejected")),
    )
    status = models.CharField(
        verbose_name=_("Status"), max_length=20, choices=STATUSES,
        default=STATUSES.pending, help_text=_("Application status")
    )
    completed = models.BooleanField(
        verbose_name=_("Completed"), default=False,
        help_text=_("Indicates if an application has been completed, including document uploads.")
    )
    semester = models.ForeignKey(
        "semesters.Semester", on_delete=models.SET_NULL, null=True, blank=True,
        help_text=_("Indicates the semester intake in which an applicant was accepted for.")
    )

    class Meta:
        verbose_name = _("Application")
        verbose_name_plural = _("Applications")
        ordering = ("-created", )

    def complete(self):
        self.completed = True
        self.save()
        return

    @property
    def full_name(self):
        return f"{self.title} {self.first_name} {self.last_name}".title()
    full_name.fget.short_description = _("Full name(s)")

    @property
    def age(self):
        today = date.today()
        age = today.year - self.date_of_birth.year - (
            (today.month, today.day) < (self.date_of_birth.month, self.date_of_birth.day))
        return age

    age.fget.short_description = _("Age")

    def accept(self, semester=None):
        if not self.status == self.STATUSES.accepted:
            self.status = self.STATUSES.accepted
            # from isppme.enrollment.models import Intake
            self.save()

            # allocate intake/Semester
            if semester:
                self.semester = semester
                self.save()
            from .tasks import notify_applicant
            notify_applicant.delay(self.id)  # TODO: Add intake period.
            return True
        return False

    @property
    def accepted(self):
        if self.status == self.STATUSES.accepted:
            return True
        return False

    def reject(self):
        self.status = self.STATUSES.rejected
        self.semester = None
        self.save()

    def acknowledge(self):
        """Marks an application as acknowledged
        Meaning an applicant has acknowledged that
        they want to be registered.
        """
        self.acknowledged = True
        self.save()
        return

    @property
    def rejected(self):
        if self.status == self.STATUSES.rejected:
            return True
        return False

    def clean(self):
        """
        Cleans input and check for irregularities
        and validates against other business rules.
        :return: None
        """

        # Check if age falls withing allowed range.
        if self.age < self.AGE_LIMIT:
            raise ValidationError(
                {'date_of_birth': _(f"Applicants should be older than {self.AGE_LIMIT} years.")},
            )

        # Make sure application has course ref.
        # Note: If a course is deleted, the corresponding
        # field in the application will be set NULL, but at
        # the time of application, a course must be selected.
        if not self.course:
            raise ValidationError(
                _("Please select a course to apply for.")
            )

        # Check for if either passport number or
        # national id exists
        if self.passport_number is None and self.national_id is None:
            raise ValidationError(
                _("Please provide either national id number or passport number.")
            )

        # Make Nationality and Country of permanent residence
        # required.
        if self.nationality is None:
            raise ValidationError(
                {'nationality': _("Please select country of nationality.")},
            )
        if self.country_of_permanent_residence is None:
            raise ValidationError(
                {'country_of_permanent_residence': _(
                    "Please select applicant's country of permanent residence")}
            )

        # Make sure either applicant provides mobile
        # number or home telephone number.
        if self.mobile_number is None and self.telephone_number is None:
            raise ValidationError(
                _("Please provide either telephone number or mobile number for contact.")
            )

        # Make sure applicants who are members provide
        # their member identification numbers(id)
        if self.is_member and (self.member_id is None or self.member_id == ""):
            raise ValidationError(
                {'member_id': _("Please provide your membership id.")}
            )

    def save(self, *args, **kwargs):
        self.full_clean()  # Trigger model validation through the Model.clean() method.
        # Implement Pre-Save(s) here
        # ----------------------------------------
        # Finally save the model instance
        super().save(*args, **kwargs)

        # Implement Post-Save(s) here
        # -----------------------------------------

    def register_as_student(self):
        """Register applicant as student.
        """
        # Deny incomplete applications
        if not self.completed:
            return


class Document(UUIDModel):
    """Stores documents related to an application
    e.g. High School Certificate etc.
    """
    application = models.ForeignKey(
        Application, on_delete=models.CASCADE, verbose_name=_("Application"), null=True, blank=True,
        related_name="documents", help_text=_("Reference to application to which these documents belong to.")
    )
    label = models.CharField(
        verbose_name=_("Label"), max_length=255,
        help_text=_(
            "Document(s) label. e.g High School Certificate or College Degree and Merit Certificate.")
    )
    file = models.FileField(
        verbose_name=_("File"), upload_to='applications/%Y/%m/',
        validators=[
            FileTypeValidator(
                allowed_types=['application/pdf', 'image/png', 'image/jpeg', 'image/bmp'],
                allowed_extensions=['.pdf', '.jpg', '.png', '.jpg', '.jpeg', '.bmp']
            ),
        ],
        help_text=_("Upload the file strictly in PDF or image formats."),
    )

    class Meta:
        verbose_name = _("Application document")
        verbose_name_plural = _("Application documents")

    def __str__(self):
        return str(self.label)

    def clean(self):
        # Make sure the file has a application
        # reference.
        if not self.application:
            raise ValidationError(
                _("Please attach these files to an application!")
            )

    def save(self, *args, **kwargs):
        self.full_clean()  # Trigger model clean
        super().save(*args, **kwargs)
