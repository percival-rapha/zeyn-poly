from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from .payment_views import check_paynow_transaction_status_view
from .views import (
    # Auth Views
    login_view, logout_view,

    # Dashboard
    dashboard_view,

    # Enrollments
    enrollment_overview_view, transcript_view,

    # Registrations
    registration_index, select_modules_view,

    # Payments
    prepare_payment_view,

    # Assignments
    assignment_details_view, assignments_list_view,

    # modules
    modules_list_view, module_assignment_list, module_detail_view,

    # Exams
    exam_listing_view, exam_calendar_view,

    # discussions
    discussions_view, discussion_details, create_discussion_view,

    # submission
    submit_assignment_view, submission_details_view, edit_submission_view,
    attach_submission_document_view,

    # chat
    chat_index_view, get_chat_url_view,

    # settings
    change_password_view,

)

urlpatterns = [
                  path(r'', login_view, name="login"),
                  path(r'logout/', logout_view, name="logout"),

                  # Dashboard
                  path(r'dashboard/', dashboard_view, name="dashboard"),

                  # Enrollments
                  path(r'enrollments/<uuid:enrollment_uuid>/',
                       enrollment_overview_view, name="enrollment_overview"),
                    path('<uuid:enrollment_uuid>/transcript/', transcript_view, name="transcript"),

                  # Registrations
                  path(r'register/<uuid:enrollment_uuid>/', registration_index, name="register"),
                  path('select-modules/<uuid:registration_uuid>/', select_modules_view, name="select_modules"),

                  # Payments
                  path('prepare-payment/<uuid:entry_uuid>/', prepare_payment_view, name="prepare_payment"),
                  path(r'check-paynow-transaction-status/<uuid:transaction_uuid>/',
                       check_paynow_transaction_status_view, name="check_paynow_transaction_status"),

                  # assignments
                  path('assignments/', assignments_list_view, name="assignments_list"),
                  path('assignments/<uuid:assignment_uuid>/', assignment_details_view, name="assignment_details"),

                  # modules
                  path('modules/', modules_list_view, name="module_listing"),
                  path('modules/<uuid:module_uuid>/', module_detail_view, name="module_details"),

                  path('modules/<uuid:module_uuid>/start-discussion/', create_discussion_view, name='create_discussion'),
                  path('modules/<uuid:module_uuid>/assignments/', module_assignment_list,
                       name="module_assignment_list"),

                  # submissions
                  path(r'assignments/<uuid:assignment_uuid>/submit/',
                       submit_assignment_view, name="submit_assignment"),
                  path(r'submissions/<uuid:submission_uuid>/',
                       submission_details_view, name="submission_details"),
                  path(r'submissions/<uuid:submission_uuid>/edit/',
                       edit_submission_view, name="edit_submission"),
                  path(r'submissions/<uuid:submission_uuid>/attach-files/',
                       attach_submission_document_view, name="attach_submission_document"),

                  # Exams
                  path('exams/', exam_listing_view, name="exam_listing"),
                  path('exam-calendar/', exam_calendar_view, name="exam_calendar"),

                  # Discussions
                  path('discussions/', discussions_view, name='discussions'),
                  path('discussions/<uuid:discussion_uuid>/<slug:title>/', discussion_details, name='discussion'),

                  # markdownx
                  path(r'markdownx/', include('markdownx.urls')),

                  # Chatter
                  path('chat-contacts/', chat_index_view, name="chat_contacts"),
                  path('chat-contacts/<int:target_user>/', get_chat_url_view, name="get_chat_url"),

                  path('chat/', include('django_chatter.urls')),

                  path('settings/change-password', change_password_view, name="change_password"),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if "debug_toolbar" in settings.INSTALLED_APPS and settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
