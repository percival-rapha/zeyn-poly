from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from model_utils.models import TimeStampedModel

from isppme.core.models import UUIDModel
from isppme.modules.models import Module


class Teacher(UUIDModel):
    """Repr(s) a tutor/lecturer
    """
    MR = 'MR'
    MRS = 'MRS'
    MS = 'MS'
    DR = 'DR'
    MISS = "MISS"
    REV = "REV"
    SR = "SR"
    PROF = "PROF"
    TITLES = (
        (MR, _("Mr")),
        (MRS, _("Mrs")),
        (MS, _("Ms")),
        (DR, _("Dr")),
        (MISS, _("Miss")),
        (REV, _("Rev")),
        (SR, _("Sr")),
        (PROF, _("Prof")),
    )
    SEX_CHOICES = (
        ("M", _("Male")),
        ("F", _("Female")),
    )
    sex = models.CharField(
        verbose_name=_("Sex"), max_length=1, choices=SEX_CHOICES,
        help_text=_("Sex")
    )
    title = models.CharField(
        verbose_name=_("Title"), max_length=10,
        help_text=_("Tutor's title"), choices=TITLES,
    )
    first_name = models.CharField(
        verbose_name=_("First name"), max_length=50,
        help_text="Tutor's legal first name"
    )
    last_name = models.CharField(
        verbose_name=_("Last name"), max_length=50,
        help_text="Tutor's legal last name"
    )
    image = models.ImageField(
        verbose_name=_("Profile Image"), null=True, blank=True,
    )
    qualifications = models.CharField(
        verbose_name=_("Qualifications"), max_length=255, null=True, blank=True,
        help_text=_("Tutor's qualifications")
    )
    modules = models.ManyToManyField(
        to=Module, verbose_name=_("Modules"), blank=True,
        help_text=_("Modules taught"), related_name="teachers"
    )
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        verbose_name=("User Account"), on_delete=models.SET_NULL, null=True,
        blank=True, related_name="teacher",
    )

    class Meta:
        verbose_name = _("Teacher")
        verbose_name_plural = _("Teachers")
        ordering = ("-created", "first_name", "last_name", )

    def __str__(self):
        return f"{self.title}. {self.first_name} {self.last_name}".title()

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    @property
    def module_count(self):
        return self.modules.count()

    @property
    def full_name(self):
        return f"{self.get_title_display()}. {self.first_name} {self.last_name}"

    @property
    def initials(self):
        return f"{self.first_name[:1]}{self.last_name[:1]}"
