from django.apps import AppConfig


class ExamsConfig(AppConfig):
    name = 'isppme.exams'
