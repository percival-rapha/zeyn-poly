from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from .views import (
    login_view, dashboard_view, logout_view,
    # Applications
    application_listings_view, single_application_view, accept_application_view,
    reject_application_view, pdf_viewer_view,

    # # Enrollments
    # enrollments_list_view,

    # Students
    student_list_view, single_student_view,

    # # Accounts & Payments
    # transaction_list_view, registration_payments_view, student_accounts_view,

    # tutors
    create_teacher_view,
    teacher_list_view,
    teacher_detail_view,
    edit_teacher_view,

    # Account settings
    password_change_view,

    # courses
    course_list_view, course_detail_view,
    module_list_view,

    # exam
    create_exam_view, exam_list_view, exam_calendar_view,
    edit_exam_view,
)

urlpatterns = [
                  path(r'', login_view, name="login"),
                  path(r'logout/', logout_view, name="logout"),
                  path('dashboard', dashboard_view, name="dashboard"),

                  # Applications
                  path('applications/', application_listings_view, name="application_listings"),
                  path('applications/<uuid:application_uuid>/', single_application_view, name="single_application"),
                  path('applications/<uuid:application_uuid>/accept/',
                       accept_application_view, name="accept_application"),
                  path('applications/<uuid:application_uuid>/reject/',
                       reject_application_view, name="reject_application"),
                  path('applications/documents/pdf-vier/<uuid:document_uuid>/', pdf_viewer_view, name="pdf_viewer"),

                  # courses
                  path(r'courses/', course_list_view, name="course_list"),
                  path(r'courses/<uuid:course_uuid>/', course_detail_view, name="course_detail"),
                  # modules
                  path(r'modules/', module_list_view, name="module_list"),

                  # Exams
                  path(r'module/<uuid:module_uuid>/exams/create', create_exam_view, name="create_exam"),

                  path(r'exams/', exam_list_view, name="exam_list"),
                  path(r'exams/calendar/', exam_calendar_view, name="exam_calendar"),
                  path(r'exams/<uuid:exam_uuid>/update/', edit_exam_view, name="edit_exam"),

                  # Enrollments
                  # path(r'enrollments/', enrollments_list_view, name="enrollments_list"),

                  # Students
                  path(r'students/', student_list_view, name="student_list"),
                  path('students/<uuid:student_uuid>/', single_student_view, name="student_detail"),

                  # # Accounts and payments
                  # path(r'transactions/', transaction_list_view, name="transaction_list"),
                  # path(r'registration-payments/', registration_payments_view, name="registration_payment_list"),
                  # path(r'student-accounts-ledger/', student_accounts_view, name="student_accounts"),
                  #
                  # Teachers
                  path(r'teachers/create/', create_teacher_view, name="create_teacher"),
                  path(r'teachers/', teacher_list_view, name="teacher_listing"),
                  path(r'teachers/<uuid:teacher_uuid>/', teacher_detail_view, name="teacher_detail"),
                  path(r'teachers/<uuid:teacher_uuid>/update/', edit_teacher_view, name="edit_teacher"),

                  # User account Settings
                  path(r'settings/change-password', password_change_view, name="change_password")

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# urlpatterns += tf_urls

if "debug_toolbar" in settings.INSTALLED_APPS:
    import debug_toolbar

    urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
