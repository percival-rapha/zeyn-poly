from django.db.models import Manager


class MerchantManager(Manager):
    use_in_migrations = True

    @staticmethod
    def get_registration_merchant():
        from .models import PaynowMerchant
        try:
            return PaynowMerchant.objects.get(
                code="ISPPME_REG",
            )
        except PaynowMerchant.DoesNotExist:
            # TODO: add code to create merchant here
            return None

    @staticmethod
    def get_tuition_merchant():
        from .models import PaynowMerchant
        try:
            return PaynowMerchant.objects.get(
                code="ISPPME_TUI",
            )
        except PaynowMerchant.DoesNotExist:
            # TODO: add code to create merchant here
            return None

    @staticmethod
    def get_exam_merchant():
        from .models import PaynowMerchant
        try:
            return PaynowMerchant.objects.get(
                code="ISPPME_EXAMS",
            )
        except PaynowMerchant.DoesNotExist:
            # TODO: add code to create merchant here
            return None

    @staticmethod
    def get_module_merchant():
        from .models import PaynowMerchant
        try:
            return PaynowMerchant.objects.get(
                code="ISPPME_MOD",
            )
        except PaynowMerchant.DoesNotExist:
            # TODO: add code to create merchant here
            return None


