from datetime import datetime, time

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.validators import ValidationError

from isppme.core.models import UUIDModel

from .managers import SemesterManager


class Semester(UUIDModel):
    """Stores a semester."""

    name = models.CharField(
        verbose_name=_("Semester name/label"), max_length=100,
        help_text=_("Semester name. e.g Summer 2019"), null=True, blank=True,
    )
    applications_start = models.DateField(
        verbose_name=_("Applications Start Date"),
    )
    registration_start = models.DateField(
        verbose_name=_("Registration opens"),
        help_text=_("Indicates when semester registration starts.")
    )
    registration_end = models.DateField(
        verbose_name=_("Registration closes"),
        help_text=_("Indicates when semester registration ends.")
    )
    start = models.DateField(
        verbose_name=_("Start"),
        help_text=_("Start date of semester.")
    )
    end = models.DateField(
        verbose_name=_("End"), help_text=_("End date of semester.")
    )

    objects = SemesterManager()

    class Meta:
        verbose_name = _("Semester")
        verbose_name_plural = _("Semesters")
        ordering = ()

    def __str__(self):
        if self.name:
            return f"{str(self.name)} ({str(self.start)} - {str(self.end)})"
        return f"{str(self.start)} - {str(self.end)}"

    @property
    def late_registration(self):
        if timezone.now().date() > self.registration_end:
            return True
        return False

    def clean(self):
        if self.start and self.end:
            if self.end < self.start:
                raise ValidationError(_("End date must be greater than start date of semester."))

            overlapping_semesters = self.__class__.objects.filter(
                end__gte=self.start
            )
            if self.pk:
                overlapping_semesters = overlapping_semesters.exclude(pk=self.pk)

            if overlapping_semesters.count() > 0:
                raise ValidationError(_("Overlapping semester error. Please correct the dates"))

    def save(self, *args, **kwargs):
        self.full_clean()  # Trigger model clean methods
        super(Semester, self).save(*args, **kwargs)  # Call super.save() method to save semester

    @property
    def start_datetime(self):
        d = datetime.combine(self.start, time(23, 00))
        return d.astimezone(tz=timezone.now().tzinfo)

    @property
    def end_datetime(self):
        d = datetime.combine(self.end, time(23, 00))
        d.astimezone(tz=timezone.now().tzinfo)
        return d.astimezone(tz=timezone.now().tzinfo)

    def register_enrollment(self, enrollment):
        if timezone.now().date() >= self.registration_start and timezone.now().date() <= self.end:
            # if enrollment.registrations.count() > 1:
            #     enrollment.complete_semester()
            # only register is current date is in semester registration range.
            if not enrollment.completed and not enrollment.deferred:
                # Only register if enrollment hasn't ended and is not currently deferred.
                enrollment.progress_semester(semester=self)
        return

    def transfer_students(self):
        from isppme.enrollments.models import Registration

        if timezone.now() > self.end:
            # Only execute if semester has ended
            # Exclude deferred students
            transferrable = self.registrations.filter(
                enrollment__deferred=False,
            )
            for reg in transferrable:
                # iterate over transferrable registrations
                if not reg.enrollment.completed and reg.can_proceed:
                    # Make sure enrollment is not complete and can proceed
                    next_semester_number = reg.enrollment.semesters_completed + 1
                    if not Registration.objects.filter(enrollment=reg.enrollment, semester_no=next_semester_number).exists():
                        # only execute if registration record does not exist already for this semester
                        reg.enrollment.complete_semester()  # Increment semesters completed
                        reg.enrollment.progress_semester()  # Advance to next semester

