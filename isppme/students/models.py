from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.validators import ValidationError

from isppme.core.models import UUIDModel
from isppme.core.countries import COUNTRIES
from isppme.users.models import get_sentinel_user

from .managers import StudentManager, gen_isppme_reg_number


class Student(UUIDModel):
    """Student record representation.
    Stores all information related to
    a student.
    """
    SEX_CHOICES = (
        ("M", _("Male")),
        ("F", _("Female")),
    )
    reg_number = models.CharField(
        verbose_name=_("Registration number"), max_length=30, default=gen_isppme_reg_number,
        help_text=_("Student registration number. Auto-generated"), editable=False,
    )

    name = models.CharField(
        verbose_name=_("Student name"), max_length=255,
        help_text=_("Full student name"),
    )

    dob = models.DateField(
        verbose_name=_("Date of birth"),
        help_text=_("Student's date of birth.")
    )

    sex = models.CharField(
        verbose_name=_("Sex"), max_length=1, choices=SEX_CHOICES,
        help_text=_("Student's sex orientation")
    )
    place_of_birth = models.CharField(
        verbose_name=_("Place of birth"), max_length=255,
        help_text=_("Student's place of birth")
    )
    national_id = models.CharField(
        verbose_name=_("National ID"), null=True, blank=True, max_length=20,
        help_text=_("Student's national id number as issued by country of nationality")
    )
    passport_number = models.CharField(
        verbose_name=_("Passport number"), null=True, blank=True, max_length=25,
        help_text=_("Student's passport number is applicable")
    )
    nationality = models.CharField(
        choices=COUNTRIES, null=True, blank=True, max_length=255,
        verbose_name=_("Nationality"),
        help_text=_("Student's nationality")
    )
    country_of_permanent_residence = models.CharField(
        choices=COUNTRIES, max_length=255, null=True, blank=True,
        help_text=_("Student's country of permanent residence")
    )
    disabilities = models.TextField(
        verbose_name=_("Disabilities"), max_length=599, blank=True, null=True,
        help_text=_("Disabilities if any")
    )
    contact_address = models.CharField(
        verbose_name=_("Contact Address"), max_length=255,
        help_text=_("Note: All correspondences will be forwarded to this address")
    )
    telephone_number = models.CharField(
        verbose_name=_("Telephone number"), max_length=20, null=True, blank=True,
        help_text=_("Student's home telephone number")
    )
    mobile_number = models.CharField(
        verbose_name=_("Mobile number"), max_length=20, null=True, blank=True,
        help_text=_("Student's mobile phone number")
    )
    email_address = models.EmailField(
        verbose_name=_("Email address"), max_length=255,
        help_text=_("Student's email address")
    )
    is_member = models.BooleanField(
        verbose_name=_("Is student a Member of ISPPME?"), default=False,
        help_text=_("Indicate if student is a member of ISPPME")
    )
    member_id = models.CharField(
        verbose_name=_("Member ID"), max_length=255, null=True, blank=True,
    )
    application_ref = models.OneToOneField(
        to='applications.Application', on_delete=models.SET_NULL, related_name="student", null=True, blank=True,
    )
    user = models.OneToOneField(
        to="users.User", on_delete=models.SET_NULL,
        verbose_name=_("User"), null=True, blank=True,
    )

    objects = StudentManager()

    def set_user(self, user_acc):
        self.user = user_acc
        self.save()

    class Meta:
        verbose_name = _("Student")
        verbose_name_plural = _("Students")

    def __str__(self):
        return f"{str(self.name)}"

    def clean(self):
        if self.dob:
            if self.dob > timezone.now().date():
                raise ValidationError({"dob": _("Date of birth cannot be greater than today.")})

    def save(self, *args, **kwargs):
        new_record = False
        if not self.pk:
            new_record = True
        self.full_clean()
        super(Student, self).save(*args, **kwargs)

    @property
    def full_name(self):
        return f"{self.name}"

    @property
    def initials(self):
        return self.name[:1]

    @property
    def balance(self):
        bal = 0
        for enr in self.enrollments.all():
            bal += enr.balance
        return bal

    @property
    def age(self):
        today = timezone.now().date()
        age = today.year - self.dob.year - (
            (today.month, today.day) < (self.dob.month, self.dob.day))
        return age

    def get_debit_ledger_entries(self):
        from isppme.accounts.models import DebitLedger

        return DebitLedger.objects.filter(
            registration__enrollment__student=self
        )

    def get_credit_ledger_entries(self):
        from isppme.accounts.models import CreditLedger

        return CreditLedger.objects.filter(
            registration__enrollment__student=self
        )

    def courses(self):
        cc = list()
        for enr in self.enrollments.all():
            cc.append(enr.course.__str__())
        return ", ".join(cc)
