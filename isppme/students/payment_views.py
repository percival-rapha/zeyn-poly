from django.shortcuts import get_object_or_404
from django.views import View
from django.http import JsonResponse

from isppme.accounts.models import Transaction


class CheckPaynowTransaction(View):

    def get(self, request, **kwargs):
        # Ajax response to return after checking
        ajax_response = dict()

        # Get Transaction
        trans = get_object_or_404(Transaction, uuid_id=kwargs["transaction_uuid"])
        # Get poll url
        trans.do_poll()
        if trans.paid:
            ajax_response.update({"complete": 1, "status": trans.status})
            trans.update_debit_ref()
        else:
            ajax_response.update({"complete": 0, "status": trans.status})


        # Finally return ajax response
        return JsonResponse(ajax_response, safe=False)


check_paynow_transaction_status_view = CheckPaynowTransaction.as_view()
