from django.contrib import admin

from .models import DebitLedger, CreditLedger, Transaction, PaynowMerchant


@admin.register(DebitLedger)
class DebitLedgerAdmin(admin.ModelAdmin):
    list_display = ("registration", "narration", "amount", "required", "paid",)


@admin.register(CreditLedger)
class CreditLedgerAdmin(admin.ModelAdmin):
    list_display = ("registration", "narration", "amount", "debit_ref", "payment_ref",)

@admin.register(PaynowMerchant)
class PNMerchantAdmin(admin.ModelAdmin):
    list_display = ("name", "code", "created", "modified", "uuid_id", )

@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ("link", "poll_url", "created", "amount_paid", "debit_ref", )
