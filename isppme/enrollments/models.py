from django.db import models, transaction, IntegrityError
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.validators import ValidationError

from isppme.core.models import UUIDModel
from isppme.students.models import Student
from isppme.courses.models import Course
from isppme.accounts.models import DebitLedger
from isppme.accounts.managers import MerchantManager
from isppme.semesters.models import Semester

from .managers import RegistrationManager

CURRENT_SEMESTER = Semester.objects.get_current_semester()

class Enrollment(UUIDModel):
    """
    Stores student enrollment(s). i.e. student->course relationship.
    If a student is studying more than one course, he/she will have
    an enrollment record corresponding to each and every course she's
    taking.
    """

    student = models.ForeignKey(
        to=Student, on_delete=models.CASCADE, related_name="enrollments",
        verbose_name=_("Student"), help_text=_("Reference to student record.")
    )
    course = models.ForeignKey(
        to=Course, on_delete=models.CASCADE, related_name="enrollments",
        verbose_name=_("Course"), help_text=_("Course reference.")
    )

    deferred = models.BooleanField(
        default=False,
    )

    class Meta:
        verbose_name = _("Enrollment")
        verbose_name_plural = _("Enrollments")
        unique_together = ("student", "course")

    def __str__(self):
        return f"{self.student.reg_number} {self.student.full_name}"

    @property
    def semesters_remaining(self):
        return self.course.semesters - self.semesters_completed

    @property
    def completed(self):
        # TODO: add more control ie. check registration records
        return self.semesters_completed >= self.course.semesters


    @property
    def semesters_completed(self):
        return self.registrations.filter(
            semester__end__lte=timezone.now().date()
        ).count()

    def progress_semester(self, semester):
        if not self.completed:
            # calculate preceding semester based on completed semesters
            semester_number = self.semesters_completed + 1

            # check if the calculated semester is greater than the actual
            # semesters studied in course
            if not semester_number > self.course.semesters:
                try:
                    with transaction.atomic():
                        record = Registration.objects.create(
                            enrollment=self,
                            semester_no=self.semesters_completed+1,
                            semester=semester,
                        )
                        record.populate_modules()
                        record.populate_fees()
                except IntegrityError:
                    # TODO: Log error
                    return None
                else:
                    return record
        return None

    @property
    def registered_for_current_semester(self):
        return self.registrations.filter(
            semester=CURRENT_SEMESTER,
        ).exists() and CURRENT_SEMESTER is not None

    @property
    def can_register(self):
        # Determines if this student enrollment can register.
        # ie. has no outstanding balances and is not registered already for current semester.

        if self.balance > 0 or self.registered_for_current_semester or self.completed:
            return False
        return True

    @property
    def balance(self):
        bal = 0
        for rc in self.registrations.all().iterator():
            bal += rc.balance
        return bal

    @staticmethod
    def add_missing_assignments(registration):
        """
        Adds missing assignments to students, in cases where
        the student has registered late or after some assignment (s)
        have been posted to a module they are studying.
        :registration:
        :return: None
        """
        from isppme.classroom.models import Assignment, RegistrationAssignments


        for md in registration.modules.all().iterator():
            assignments = Assignment.objects.filter(
                module=md.module,
                created__range=(registration.semester.start_datetime, registration.semester.end_datetime)
            ).prefetch_related()
            for assignment in assignments.iterator():
                qs = RegistrationAssignments.objects.filter(registration=registration, assignment=assignment)
                if not qs.exists():
                    try:
                        ass_entry = RegistrationAssignments(
                            registration=registration,
                            assignment=assignment,
                        )
                        ass_entry.save()
                    except Exception as ex:
                        pass

    @staticmethod
    def add_missing_exams(registration):
        """
        Adds missing assignments to students, in cases where
        the student has registered late or after some assignment (s)
        have been posted to a module they are studying.
        :registration:
        :return: None
        """
        from isppme.exams.models import Exam, RegistrationExam


        for md in registration.modules.all():
            exams = Exam.objects.filter(
                module=md.module,
                semester=registration.semester,
            )
            for exam in exams:
                qs = RegistrationExam.objects.filter(registration=registration, exam=exam)
                if not qs.exists():
                    try:
                        ex_entry = RegistrationExam(
                            registration=registration,
                            exam=exam,
                        )
                        ex_entry.save()
                    except Exception as ex:
                        pass


class EnrollmentSemesters(UUIDModel):
    """Stores semesters done"""

    enrollment = models.ForeignKey(
        to="enrollments.Enrollment", on_delete=models.CASCADE, related_name="semesters",
        verbose_name=_("Enrollment"), help_text=_("Reference to enrollent record.")
    )

    semester_no = models.PositiveSmallIntegerField(
        verbose_name=_("Semester number"), default=1,
        help_text=_("Indicates the semester number.")
    )

    semester = models.ForeignKey(
        to="semesters.Semester", on_delete=models.CASCADE, related_name="enrollments"
    )

    registration_record = models.ForeignKey(
        to="enrollments.Registration", on_delete=models.SET_NULL, related_name="enrollment_semesters", null=True,
        blank=True,
        verbose_name=_("Registration record"), help_text=_("Points to the registration record for that semester.")
    )

    class Meta:
        verbose_name = _("Enrollment semester")
        verbose_name_plural = _("Enrollment semesters")
        unique_together = (
            ("enrollment", "semester_no"),
            ("enrollment", "semester_no", "registration_record"),
            ("enrollment", "registration_record"),
        )

    def clean(self):
        if self.semester_no and self.enrollment:
            if self.semester_no > self.enrollment.course.semesters:
                raise ValidationError(_("Semester number out of range."))

    def save(self, *args, **kwargs):
        self.full_clean()
        super(EnrollmentSemesters, self).save(*args, **kwargs)


class Registration(UUIDModel):
    """
    Stores registration records for each semester a student
    is studying.
    """

    enrollment = models.ForeignKey(
        to='enrollments.Enrollment', on_delete=models.CASCADE, related_name="registrations",
        verbose_name=_("Enrollment"),
        help_text=_("Enrollment record reference i.e. pointer to student enrollment record.")
    )
    semester_no = models.PositiveSmallIntegerField(
        verbose_name=_("Semester number"),
        help_text=_("Indicates the number of the semester in which this student is registering.")
    )
    semester = models.ForeignKey(
        to='semesters.Semester', on_delete=models.CASCADE, related_name="registrations",
        verbose_name=_("Semester"),
        help_text=_("Semester reference. This show the actual semester in terms of dates this registration is in.")
    )
    complete = models.BooleanField(
        verbose_name=_("Complete"), default=False, editable=False,
        help_text=_("Indicates if student has completed registration by paying fees")
    )

    # Field to track if student has chosed and finalised module choices
    # in cases where the course in that particular semester allows students to choose
    # modules they want to study.
    modules_confirmed = models.BooleanField(
        verbose_name=_("Modules confirmed"), editable=False, default=False,
    )

    objects = RegistrationManager()

    class Meta:
        verbose_name = _("Registration record")
        verbose_name_plural = _("Registration records")
        unique_together = (
            ("enrollment", "semester_no"),  # Make sure student cannot be registered in same semester no. more than once
            ("enrollment", "semester"),  # Make sure student cannot be registered in same semester more than once
        )
        ordering = ("semester_no", )

    def clean(self):
        # Validate semester_no
        # Semester number cannot be greater than
        # the semesters a course takes to complete.
        course_semester_count = self.enrollment.course.semesters
        if self.semester_no:
            if self.semester_no > course_semester_count:
                raise ValidationError(
                    _(
                        f"Semester number invalid. This enrollment record's course has {course_semester_count} semester(s).")
                )

    def save(self, *args, **kwargs):
        self.full_clean()
        super(Registration, self).save(*args, **kwargs)

    def populate_fees(self):
        # 1. Registration Fees
        award = self.enrollment.course.award
        course = self.enrollment.course
        reg_entry = DebitLedger(
            registration=self,
            narration=f"{str(award.name)} Registration Fee",
            merchant_code="ISPPME_REG",
            amount=award.registration_fee,
        )
        reg_entry.save()
 
        # 1.2 Late reg fees
        if self.semester:
            if timezone.now().date() > self.semester.registration_end:
                late_reg_entry = DebitLedger(
                    registration=self,
                    narration=f"{str(award.name)} late registration Fee",
                    merchant_code="ISPPME_REG",
                    amount=award.late_registration_fee,
                ).save()

        # 2. Tuition/School Fee(s)
        try:
            fees = course.semester_setup.get(semester_no=self.semester_no)
            tuition_entry = DebitLedger(
                registration=self,
                narration=f"{str(self.enrollment.course.__str__())} Tuition Fee",
                merchant_code="ISPPME_TUI",
                amount=fees.fee,
            )
            tuition_entry.save()
        except Exception as ex:
            # TODO: log error
            pass

    def populate_modules(self):
        # 1. Populate modules retaken first.
        if self.semester_no > 1:
            try:
                previous_registration_record = self.__class__.objects.get(semester_no=self.semester_no - 1, enrollment=self.enrollment)
            except Exception as ex:
                # TODO: log error
                pass
            else:
                if previous_registration_record:
                    for md in previous_registration_record.modules.all():
                        if md.score < 50:
                            RegisteredModules(
                                registration_record=self,
                                module=md.module,
                                compulsory=True,
                                core=md.core,
                            ).save()

        # 2. Populate standard semester modules
        standard_semester_modules = self.enrollment.course.course_modules.filter(
            semester_no=self.semester_no
        )
        if standard_semester_modules.count() > 0:
            # Add compulsory
            compulsory_modules = standard_semester_modules.filter(compulsory=True)
            for cmd in compulsory_modules:
                RegisteredModules(
                    registration_record=self,
                    module=cmd.module,
                    compulsory=cmd.compulsory,
                    core=cmd.core,
                ).save()

            # Add other non-compulsory models
            non_compulsory_modules = standard_semester_modules.filter(compulsory=False)
            if non_compulsory_modules.count() > 0:
                # If this semester has non-compulsory modules, wait for student to
                # confirm the modules he/she wants to study.
                self.modules_confirmed = False
                self.save()
                # Add all modules by default ans wait for confirmation.
                for ncmd in non_compulsory_modules:
                    RegisteredModules(
                        registration_record=self,
                        module=ncmd.module,
                        compulsory=False,
                        core=ncmd.core,
                    ).save()
            else:
                # Confirm modules if there aren't any non-compulsory modules.
                # Note: This will debit the student's account for this
                # current registration with the required fees (exam, module) per
                # module to be studied.
                self.confirm_modules()



    def confirm_modules(self):
        if not self.modules.count() > 0:
            # TODO: log case
            return
        else:
            award = self.enrollment.course.award
            module_count = self.modules.count()

            total_module_fees = module_count * award.module_fee
            total_exam_fees = module_count * award.exam_fee

            # Debit module fees
            DebitLedger(
                narration=f"x{module_count} {str(award.name)} module fees",
                registration=self,
                amount=total_module_fees,
                required=True,
                merchant_code="ISPPME_MOD",
            ).save()

            # Debit exam fees
            DebitLedger(
                narration=f"x{module_count} {str(award.name)} exam fees",
                registration=self,
                amount=total_exam_fees,
                merchant_code="ISPPME_EXAMS",
                required=False,
            ).save()

            self.modules_confirmed = True
            self.save()

    @property
    def required_modules(self):
        return self.enrollment.course.semester_setup.get(
            semester_no=self.semester_no
        ).no_of_modules

    @property
    def compulsory_modules(self):
        return self.enrollment.course.course_modules.filter(
            semester_no=self.semester_no, compulsory=True,
        ).count()

    @property
    def total_modules(self):
        return self.enrollment.course.course_modules.count()

    @property
    def modules_to_choose(self):
        return self.required_modules - self.compulsory_modules

    def get_optional_modules(self):
        return self.modules.filter(
            compulsory=False,
        )

    def get_compulsory_modules(self):
        return self.modules.filter(
            compulsory=True
        )

    @property
    def can_proceed(self):
        """Evaluates if student can proceed to next
        semester
        """
        if self.enrollment.completed:
            return False
        return True

    @property
    def balance(self):
        owing = 0
        for de in self.debit_ledger_entries.all():
            owing += de.amount

        paid = 0
        unique_transaction_entries = list()
        for ce in self.credit_ledger_entries.all():
            if ce.transaction:
                if not ce.transaction in unique_transaction_entries:
                    unique_transaction_entries.append(ce.transaction)
                    paid += ce.amount
            else:
                paid += ce.amount

        return owing - paid

    def apply_module_choices(self, choices):
        # remove old default
        for md in self.modules.filter(compulsory=False):
            if md not in choices:
                md.delete()
        self.confirm_modules()
        return


class RegisteredModules(UUIDModel):
    """Stores modules registered by student"""

    registration_record = models.ForeignKey(
        to=Registration, on_delete=models.CASCADE, related_name="modules",
        verbose_name=_("Registration record"), help_text=_('Reference to the registration record')
    )

    module = models.ForeignKey(
        to='modules.Module', on_delete=models.CASCADE, related_name="registrations",
        verbose_name=_("Module")
    )

    compulsory = models.BooleanField(
        verbose_name=_("Compulsory"), default=True,
        help_text=_("Indicates if this module is compulsory to be taken.")
    )

    core = models.BooleanField(
        verbose_name=_("Core"), default=True,
        help_text=_("Indicates if this is a core module.")
    )

    class Meta:
        verbose_name = _("Registered module")
        verbose_name_plural = _("Registered modules")
        ordering = ("module", )
        unique_together = ("registration_record", "module")

    def get_peers(self):
        other_reg_modules = self.__class__.objects.filter(
            module=self.module, registration_record__semester=self.registration_record.semester,
        ).exclude(id=self.id)
        return [r.registration_record.enrollment.student for r in other_reg_modules]

    def __str__(self):
        return f"{str(self.module.name)}"

    @property
    def exam_total(self):
        try:
            exam = self.registration_record.exams.get(exam__module=self.module)
        except Exception as ex:
            # TODO: Log error
            return 0
        else:
            total = (exam.score / exam.exam.points) * self.module.exam_contribution
            return round(total, 1)

    exam_total.fget.short_description = _("Exam total")

    @property
    def continuous_assessment(self):
        if not self.module.continuous_assessment_contribution == 0:
            assignments = self.registration_record.assignments.filter(assignment__module=self.module,
                                                                      assignment__exclude_in_ca=False)
            if assignments.count() > 0:
                grand_total = 0
                score_total = 0
                for assignment in assignments.iterator():
                    grand_total += assignment.assignment.points
                    score_total += assignment.score
                total = (score_total / grand_total) * self.module.continuous_assessment_contribution
                return round(total, 1)
        return 0

    continuous_assessment.fget.short_description = _("Continuous assessment")

    @property
    def score(self):
        if self.registration_record.semester.end > timezone.now().date():
            return "Pending"
        else:
            exam_total = self.exam_total
            if self.module.continuous_assessment_contribution > 0:
                total = exam_total + self.continuous_assessment
            else:
                total = exam_total
            return round(total, 1)

    score.fget.short_description = _("Score")

    @property
    def classification(self):
        if self.registration_record.semester.end > timezone.now().date():
            return "Pending"
        else:
            score = self.score
            if score < 50:
                return "Fail"
            else:
                if score >= 50 and score <= 69:
                    return "Pass"
                else:
                    if score >= 70 and score <= 79:
                        return "Merit"
                    else:
                        return "Distinction"

    @property
    def decision(self):
        if self.registration_record.semester.end > timezone.now().date():
            return "Pending"
        else:
            if self.score < 50:
                return "RETAKE"


    @property
    def decision_code(self):
        if self.registration_record.semester.end > timezone.now().date():
            return "Pending"
        else:
            if self.score < 50:
                return "R"
