from django.contrib.auth.mixins import AccessMixin

from django_hosts.resolvers import reverse as hosts_reverse

from isppme.semesters.models import Semester


class StudentLoginRequired(AccessMixin):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        else:
            if not request.user.is_student:
                return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

    def get_login_url(self):
        return hosts_reverse('login', host='students',)


class StudentContextMixin:

    def __init__(self, **kwargs):
        self.registration_record = None
        self.current_semester = None
        super(StudentContextMixin, self).__init__(**kwargs)

    def get_context_data(self, **kwargs):
        CURRENT_SEMESTER = Semester.objects.get_current_semester()
        context = super().get_context_data(**kwargs)
        context["student"] = self.request.user.student
        context["SEMESTER"] = CURRENT_SEMESTER
        return context

    def dispatch(self, request, *args, **kwargs):
        CURRENT_SEMESTER = Semester.objects.get_current_semester()
        if CURRENT_SEMESTER:
            self.current_semester = CURRENT_SEMESTER
        from isppme.enrollments.models import Registration
        try:
            self.registration_record = Registration.objects.get(
                semester=CURRENT_SEMESTER, enrollment__student=self.request.user.student
            )
        except Exception as ex:
            # TODO: log exception
            pass
        return super().dispatch(request, *args, **kwargs)

class HasZeroBalance(AccessMixin):
    """
    Deny a request with a permission error if the student
    has an outstanding balance on their account.
    """

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
