from django import forms
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm, UserCreationForm
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model

from crispy_forms.layout import Layout, Field, Fieldset, Div, HTML, ButtonHolder, Submit
from crispy_forms.helper import FormHelper

from isppme.classroom.models import Assignment, AssignmentDocument, Review
from isppme.exams.models import Exam
from isppme.modules.models import MaterialUploads

from .models import Teacher

User = get_user_model()


class AddTutorForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = [
            "title", "first_name", "last_name", "sex", "qualifications", "modules"
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Submit'))


class TeacherAuthenticationForm(AuthenticationForm):
    def confirm_login_allowed(self, user):
        if not user.is_active:
            raise forms.ValidationError(
                _("This account is inactive."),
                code='inactive',
            )
        if not user.is_teacher:
            raise forms.ValidationError(
                _("Account not allowed here."), code="not_allowed"
            )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-prepended'


class CreateTeacherAccountForm(UserCreationForm):
    id = forms.CharField(
        max_length=30, label=_(u"ISPPME ID"), help_text=_(u"ISPPME ID as provided by ISPPME.")
    )
    title = forms.CharField(
        max_length=None, widget=forms.Select(choices=Teacher.TITLES, ),
        help_text=_(u"Title as reflected on ISPPME record.")
    )
    first_name = forms.CharField(
        max_length=255, help_text=_(u"First name(s) as reflected on ISPPME record.")
    )
    last_name = forms.CharField(
        max_length=255, help_text=_(u"Last name(s) as reflected on ISPPME record.")
    )

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ("username", "email",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Submit'))

        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-prepended'

        self.order_fields(["id", "title", "first_name", "last_name",
                           "username", "email", "password1", "password1", ])

    def clean(self):
        data = super().clean()
        id = data.get("id", "")
        title = data.get("title", "")
        last_name = data.get("last_name", "")
        first_name = data.get("first_name", "")

        if id and title and last_name and first_name:
            try:
                t = Teacher.objects.get(id=id, title=title,
                                        first_name=first_name, last_name=last_name)
                if t.user:
                    self.add_error(
                        None, "Tutor account already exists. Please Login to access your account.")
                else:
                    self.teacher_record = t
            except Exception as ex:
                self.add_error(None, "Teacher record not found. Please make sure you have a "
                                     "Teacher record created.")


class CustomPasswordChangeForm(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Submit'))


class AssignmentForm(forms.ModelForm):
    """Form for creating/uploading an
    assignment.
    """

    class Meta:
        model = Assignment
        fields = [
            "module", "label", "due_date", "due_time", "exclude_in_ca", "points", "description",
        ]
        widgets = {
            "module": forms.HiddenInput(),
            "due_date": forms.DateInput(attrs={"data-toggle": "flatpickr"}),
            "due_time": forms.TimeInput(
                attrs={
                    "data-toggle": "flatpickr",
                    "data-flatpickr-enable-time": "true",
                    "data-flatpickr-no-calendar": "true",
                    "data-flatpickr-alt-format": "H:i",
                    "data-flatpickr-date-format": "H:i"
                }
            )
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.add_input(Submit("submit", "Submit"))


class AssignmentDocumentForm(forms.ModelForm):
    class Meta:
        model = AssignmentDocument
        fields = [
            "label", "assignment", "file",
        ]
        widgets = {
            "assignment": forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.add_input(Submit("submit", "Submit"))


class ReviewForm(forms.ModelForm):
    """Form to save a review or an assessment
    of an assignment submission."""

    class Meta:
        model = Review
        fields = [
            "submission", "points", "remarks",
        ]
        widgets = {
            "submission": forms.HiddenInput(),
            "points": forms.NumberInput(
                attrs={"class": "form-control", "min": 1, "placeholder": "Points"}
            )
        }


class StudyMaterialForm(forms.ModelForm):
    class Meta:
        model = MaterialUploads
        fields = ["module_ref", "name", "file", "url", ]
        widgets = {
            "module_ref": forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.add_input(Submit("submit", "Submit"))
