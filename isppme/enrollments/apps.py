from django.apps import AppConfig


class EnrollmentsConfig(AppConfig):
    name = 'isppme.enrollments'
