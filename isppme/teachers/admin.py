from django.contrib import admin

from .models import Teacher

@admin.register(Teacher)
class TAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "first_name", "last_name", "created", )
