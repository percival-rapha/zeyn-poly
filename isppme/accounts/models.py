import requests
from six.moves.urllib_parse import quote_plus, parse_qs

from django.db import models
from django.utils.translation import ugettext_lazy as _

from paynow import StatusResponse

from isppme.core.models import UUIDModel


class DebitLedger(UUIDModel):
    """Stores ledger entries made to a student's record. These might include
    Tuition, Registration Fees, Exam Fees etc.
    This model only stores debited items. Credits/Payments have their own model.
    """

    registration = models.ForeignKey(
        to="enrollments.Registration", on_delete=models.CASCADE, related_name="debit_ledger_entries",
        verbose_name=_("Student"), help_text=_("Reference to student registration record"),
    )

    narration = models.CharField(
        verbose_name=_("Narration"), max_length=255,
        help_text=_("Entry label or narration e.g. Semester 1 Tuition.")
    )

    amount = models.DecimalField(
        verbose_name=_("Amount"), max_digits=19, decimal_places=2,
        help_text=_("Amount debited")
    )
    merchant_code = models.CharField(
        verbose_name='Paynow merchant code', max_length=255,
    )

    required = models.BooleanField(
        verbose_name=_("Required"), default=True, editable=False,
        help_text=_("Indicates if this fee entry is required for the student to access portal privileges.")
    )

    class Meta:
        verbose_name = _("Debit ledger entry")
        verbose_name_plural = _("Debit ledger entries")
        ordering = ("created",)

    def __str__(self):
        return f"{str(self.narration)} - ${str(self.amount)}"

    @property
    def merchant(self):
        from isppme.accounts.models import PaynowMerchant
        qs = PaynowMerchant.objects.filter(
            code=self.merchant_code
        )
        if qs.exists():
            return qs[0]
        return None
    

    @property
    def paid(self):
        credit_entries = self.credit_entries.all()
        if credit_entries.count() > 0:
            paid_total = 0
            for entry in credit_entries:
                paid_total += entry.amount
            if paid_total < self.amount:
                return False
            return True
        return False


class CreditLedger(UUIDModel):
    """Stores ledger entries made to a student's record. These might include
    Tuition, Registration Fees, Exam Fees etc.
    This model only stores credit items. Debits/Deductions have their own model.
    """
    registration = models.ForeignKey(
        to="enrollments.Registration", on_delete=models.CASCADE, related_name="credit_ledger_entries",
        verbose_name=_("Student"), help_text=_("Reference to student registration record"),
    )

    debit_ref = models.ForeignKey(
        to='accounts.DebitLedger', on_delete=models.SET_NULL, related_name="credit_entries",
        verbose_name=_("Debit reference"), help_text=_("Reference to debit entry"), null=True, blank=True,
    )

    narration = models.CharField(
        verbose_name=_("Narration"), max_length=255,
        help_text=_("Entry label or narration e.g. Semester 1 Tuition.")
    )

    amount = models.DecimalField(
        verbose_name=_("Amount"), max_digits=19, decimal_places=2,
        help_text=_("Amount debited")
    )
    transaction = models.ForeignKey(
        to="accounts.Transaction", on_delete=models.SET_NULL, related_name="payments",
        null=True, blank=True, verbose_name=_("Transactions"),
    )
    payment_ref = models.CharField(
        verbose_name=_("Payment reference"), max_length=255,
        help_text=_("Stores payment reference i.e. Proof of payment or check no."),
    )

    class Meta:
        verbose_name = _("Credit ledger entry")
        verbose_name_plural = _("Credit ledger entries")
        ordering = ("transaction", "-created",)

    def __str__(self):
        return f"{str(self.narration)} - ${str(self.amount)}"


class PaynowMerchant(UUIDModel):
    code = models.CharField(
        verbose_name=_('Code'), unique=True, max_length=255,
    )
    name = models.CharField(
        verbose_name=_("Name"), max_length=255,
        help_text=_("Credentials name or destination account name to identify with.")
    )
    integration_id = models.PositiveIntegerField(
        verbose_name=_("Integration ID"),
        help_text=_("Paynow integration ID as provided by Paynow.")
    )
    integration_key = models.CharField(
        verbose_name=_("Integration Key"), max_length=255,
        help_text=_("Paynow integration key as provided by Paynow.")
    )

    class Meta:
        verbose_name = _("ISPPME Paynow merchant")
        verbose_name_plural = _("ISPPME Paynow merchants")

    def __str__(self):
        return f"{str(self.name)}"


class Transaction(UUIDModel):
    """Stores paynow variables
    """

    debit_ref = models.ForeignKey(
        to='accounts.DebitLedger', verbose_name=_("Debit ref."), on_delete=models.SET_NULL,
        related_name="transactions", null=True, blank=True,
    )
    link = models.CharField(
        verbose_name=_("Link"), editable=False, max_length=512,
    )
    poll_url = models.CharField(
        verbose_name=_("Poll url"), editable=False, max_length=512,
    )
    amount = models.DecimalField(max_digits=19, decimal_places=2, null=True, blank=True)


    class Meta:
        verbose_name = _("Paynow transaction")
        verbose_name_plural = _("Paynow transactions")
        ordering = ("-created",)

    @property
    def amount_paid(self):
        # Get paid amount from paynow POLL_URL
        status = self.do_poll()
        if status:
            if status.paid:
                return status.amount
            return 0
        return 0

    @property
    def status(self):
        stat_obj = self.do_poll()
        if stat_obj:
            return stat_obj.status
        return "Error"

    def do_poll(self):
        # Get poll url
        poll_url = self.poll_url
        if poll_url:
            response = requests.post(poll_url, {})
            if response.status_code == 200:
                # Get query string from response
                response_qs = parse_qs(response.text)
                # create_holding_dict
                res = dict()
                # loop query string
                for key, value in response_qs.items():
                    res.update({key: str(value[0])})
                status = StatusResponse(res, False)  # Get StatusResponse object from Paynow
                if status.paid:
                    self.amount = status.amount
                    self.save()
                return status
        return None

    @property
    def paid(self):
        status = self.do_poll()
        if status:
            if status.paid:
                return True
        return False

    def update_debit_ref(self):
        if CreditLedger.objects.filter(transaction=self).exists():
            return
        else:
            if self.paid:
                CreditLedger.objects.create(
                    registration=self.debit_ref.registration,
                    debit_ref=self.debit_ref,
                    narration=self.debit_ref.narration,
                    amount=self.amount_paid,
                    transaction=self,
                    payment_ref="Paynow"
                )
            return
