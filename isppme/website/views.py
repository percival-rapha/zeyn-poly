from django.shortcuts import render
from django.views import generic, View

from isppme.courses.models import Course
from isppme.events.models import Event

from isppme.website.models import DownloadCategory


class Index(generic.TemplateView):
    template_name = "website/index.html"

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["event"] = Event.objects.get_featured_event()
        return ctx


index_view = Index.as_view()


class EventDetails(generic.DetailView):
    model = Event
    template_name = "website/event_details.html"
    context_object_name = "event"


event_details_view = EventDetails.as_view()


class Events(generic.ListView):
    template_name = "website/events_list.html"
    model = Event
    context_object_name = "events"
    paginate_by = 25


events_view = Events.as_view()


class DownloadView(generic.ListView):
    model = DownloadCategory
    template_name = "website/downloads.html"
    context_object_name = "dlcs"


download_view = DownloadView.as_view()


class MembershipView(generic.TemplateView):
    template_name = "website/membership.html"


membership_view = MembershipView.as_view()


class AboutUsView(generic.TemplateView):
    template_name = "website/about_us.html"


about_us_view = AboutUsView.as_view()


class ContactUsView(generic.TemplateView):
    template_name = "website/contact_us.html"


contact_us_view = ContactUsView.as_view()


class CoursesView(generic.ListView):
    template_name = "website/courses.html"
    model = Course
    context_object_name = "courses"


courses_view = CoursesView.as_view()


class CourseDetails(generic.DetailView):
    model = Course
    context_object_name = "course"
    template_name = "website/course_details.html"


course_details_view = CourseDetails.as_view()


class WomenDesk(generic.TemplateView):
    template_name = "website/womens_desk.html"


women_desk_view = WomenDesk.as_view()
