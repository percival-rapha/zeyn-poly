"""
Contains filters (django-filter.FilterSet) used
in the staff app.
"""

from django.utils.translation import ugettext_lazy as _

import django_filters

from isppme.students.models import Student
from isppme.applications.models import Application
# from isppme.courses.models import Qualification, Course
# from isppme.enrollment.models import Enrollment, Semester
# from isppme.accounts.models import Transaction, RegistrationPayment, Ledger
from isppme.teachers.models import Teacher
from isppme.modules.models import Module


class StudentFilter(django_filters.FilterSet):
    reg_number = django_filters.CharFilter(lookup_expr='icontains')
    name = django_filters.CharFilter(lookup_expr='icontains')
    sex = django_filters.ChoiceFilter(choices=Student.SEX_CHOICES,
                                      empty_label=_(u'Any'))
    is_member = django_filters.BooleanFilter(label=_("Member Status"),)

    class Meta:
        model = Student
        fields = [
            "reg_number", "name",
        ]


class ApplicationFilter(django_filters.FilterSet):
    """Filters Applications by diff.
    variables.
    """
    first_name = django_filters.CharFilter(lookup_expr='icontains')
    last_name = django_filters.CharFilter(lookup_expr='icontains')
    entry_type = django_filters.ChoiceFilter(choices=Application.ENTRY_TYPES, empty_label=_(u"Any"))
    status = django_filters.ChoiceFilter(choices=Application.STATUSES, empty_label=_(u"Any"))
    is_member = django_filters.BooleanFilter(label=_("Member Status"),)
    # course__qualification = django_filters.ModelChoiceFilter(
    #     empty_label=_(u"Any"), queryset=Qualification.objects.all(), label=_("Qualification"))

    class Meta:
        model = Application
        fields = [
            "first_name", "last_name", "status", "entry_type", "is_member", "course__award"
        ]
#
#
# class EnrollmentFilter(django_filters.FilterSet):
#     """Filters enrollment records
#     """
#
#     semester = django_filters.ModelChoiceFilter(
#         empty_label=_(u"Any"), queryset=Semester.objects.all(), label=_("Introductory Semester")
#     )
#     course__course = django_filters.ModelChoiceFilter(
#         empty_label=_(u"Any"), queryset=Course.objects.all(), label=_(u"Course")
#     )
#     student__reg_number = django_filters.CharFilter(
#         label=_(u"Student Reg. Number"), lookup_expr='icontains'
#     )
#
#     class Meta:
#         model = Enrollment
#         fields = [
#             "semester", "course__course", "student__reg_number",
#         ]

#
# class TransactionFilter(django_filters.FilterSet):
#     """Filters transactions (Paynow)"""
#     reference = django_filters.CharFilter(
#         lookup_expr="icontains", label=_(u"Reference:&nbsp;")
#     )
#     completed = django_filters.BooleanFilter(
#         field_name="completed", label=_(u"Completed?:&nbsp;"),
#     )
#
#     class Meta:
#         model = Transaction
#         fields = [
#             "reference", "completed",
#         ]


# class RegPaymentsFilter(django_filters.FilterSet):
#     """Registration records filter
#     """
#     application__first_name = django_filters.CharFilter(
#         label=_(u"Applicant name:&nbsp;"), lookup_expr='icontains',
#     )
#     application__last_name = django_filters.CharFilter(
#         label=_(u"Applicant last name:&nbsp;"), lookup_expr='icontains',
#     )
#
#     class Meta:
#         model = RegistrationPayment
#         fields = [
#             "application__first_name", "application__last_name",
#         ]
#
#
# class CustomRangeWidget(django_filters.widgets.RangeWidget):
#     def __init__(self, from_attrs=None, to_attrs=None, attrs=None):
#         super().__init__(attrs)
#
#         if from_attrs:
#             self.widgets[0].attrs.update(from_attrs)
#         if to_attrs:
#             self.widgets[1].attrs.update(to_attrs)
#
#
# class LedgerFilter(django_filters.FilterSet):
#     """FilterSet for Ledger entries
#     """
#
#     account__reg_number = django_filters.CharFilter(
#         label=_(u"Student"), lookup_expr='icontains',
#     )
#     narration = django_filters.CharFilter(
#         label=_(u"Narration"), lookup_expr='icontains',
#     )
#     created = django_filters.DateFromToRangeFilter(
#         widget=CustomRangeWidget(
#             attrs={
#                 'data-toggle': "flatpickr",
#             },
#             from_attrs={
#                 'placeholder': "From"
#             },
#             to_attrs={
#                 "placeholder": 'To'
#             }
#         )
#     )
#
#     class Meta:
#         model = Ledger
#         fields = [
#             "account__reg_number", "narration", "created",
#         ]


class TeacherFilter(django_filters.FilterSet):
    """Filters Tutor Records
    """
    first_name = django_filters.CharFilter(lookup_expr='icontains', label="First name: &nbsp;")
    last_name = django_filters.CharFilter(lookup_expr='icontains', label="Last name: &nbsp;")

    class Meta:
        model = Teacher
        fields = [
            "first_name", "last_name",
        ]


class ModuleFilter(django_filters.FilterSet):

    class Meta:
        model = Module
        fields = [
            "code", "name",
        ]
