from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'isppme.accounts'
