import html

from django.db import models
from django.utils.translation import ugettext_lazy as _

from markdownx.models import MarkdownxField
from markdownx.utils import markdownify

from isppme.core.models import UUIDModel

from .managers import EventManager


class Event(UUIDModel):
    name = models.CharField(
        verbose_name=_("Name"), max_length=255
    )
    venue = models.CharField(
        verbose_name=_("Venue"), max_length=255
    )
    start_date = models.DateField(
        verbose_name=_("Start date")
    )
    start_time = models.TimeField(
        verbose_name=_("Start time")
    )
    end_time = models.TimeField(
        verbose_name=_("End time")
    )
    details = MarkdownxField(
        verbose_name=_("Details"),
    )

    objects = EventManager()

    class Meta:
        verbose_name = _("Event")
        verbose_name_plural = _("Events")
        ordering = ("start_date",)

    def __str__(self):
        return self.name

    @property
    def details_markdownified(self):
        return markdownify(html.escape(self.details))

    @property
    def get_first_dn(self):
        return str(self.start_date.strftime("%d"))[0]

    @property
    def get_second_dn(self):
        return str(self.start_date.strftime("%d"))[1]
