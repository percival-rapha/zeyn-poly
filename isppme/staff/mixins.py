from django.contrib.auth.mixins import AccessMixin

from isppme.semesters.models import Semester

CURRENT_SEMESTER = Semester.objects.get_current_semester()

from django_hosts.resolvers import reverse as hosts_reverse


class StaffLoginRequiredMixin(AccessMixin):
    """Verify that the current user is authenticated and is staff."""

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        if not request.user.is_staff:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

    def get_login_url(self):
        return hosts_reverse('login', host='staff', )


class StaffContextMixin:
    def __init__(self, **kwargs):
        self.current_semester = None
        super(StaffContextMixin, self).__init__(**kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["SEMESTER"] = CURRENT_SEMESTER
        return context

    def dispatch(self, request, *args, **kwargs):
        if CURRENT_SEMESTER:
            self.current_semester = CURRENT_SEMESTER
        return super().dispatch(request, *args, **kwargs)
