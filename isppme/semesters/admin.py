from django.contrib import admin

from .models import Semester


@admin.register(Semester)
class SemesterAdmin(admin.ModelAdmin):
    list_display = ("name", "registration_start", "registration_end", "start", "end",)
