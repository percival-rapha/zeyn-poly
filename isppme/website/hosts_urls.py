from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from .views import (
    index_view, about_us_view, contact_us_view,
    courses_view, course_details_view, women_desk_view,
    event_details_view, events_view, membership_view,
    download_view,
)

urlpatterns = [
                  path(r'', index_view, name="index"),
                  path('about-us/', about_us_view, name="about_us"),
                  path('contact-us/', contact_us_view, name="contact_us"),
                  path('courses/', courses_view, name="courses"),
                  path('courses/<int:pk>-<slug:slug>/', course_details_view, name="course_details"),
                  path('womens-desk/', women_desk_view, name="women_desk"),
                  path('events/', events_view, name="events"),
                  path('events/<int:pk>-<slug:slug>/', event_details_view, name="event_details"),
                  path('membership/', membership_view, name="membership"),
                  path('downloads/', download_view, name="downloads"),
                  # markdownx
                  path(r'markdownx/', include('markdownx.urls')),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if "debug_toolbar" in settings.INSTALLED_APPS and settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
