from django.apps import AppConfig


class WebsiteConfig(AppConfig):
    name = 'isppme.website'
