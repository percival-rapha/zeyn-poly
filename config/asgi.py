"""
ASGI entrypoint. Configures Django and then runs the application
defined in the ASGI_APPLICATION setting.
"""

import os
import environ
import sys
import django
from channels.routing import get_default_application

from django.conf import settings

app_path = os.path.abspath(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
)
sys.path.append(os.path.join(app_path, "isppme"))

env = environ.Env()

# https://github.com/twoscoops/django-twoscoops-project/blob/develop/project_name/project_name/settings/production.py#L14-L21
def get_env_setting(setting):
    """ Get the environment setting or return exception """
    try:
        return os.environ[setting]
    except KeyError:
        error_msg = "Set the %s env variable" % setting
        raise ImproperlyConfigured(error_msg)

os.environ.setdefault(
	'DJANGO_SETTINGS_MODULE', env(
		'DJANGO_SETTINGS_MODULE', default='config.settings.local'
		)
	)

django.setup()
application = get_default_application()

"""
ASGI config for isppme project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""
