from django.contrib import admin

from .models import Application, Document


@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    list_display = ("full_name", "age", "entry_type", "course", "created", "status",)


@admin.register(Document)
class DocAdmin(admin.ModelAdmin):
    list_display = ("application", "label", "file",)
