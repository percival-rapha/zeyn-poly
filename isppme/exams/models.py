import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.validators import ValidationError

from isppme.core.models import UUIDModel
from isppme.modules.models import Module


class Exam(UUIDModel):
    """Stores an exam record"""

    module = models.ForeignKey(
        to=Module, on_delete=models.CASCADE, related_name="exams",
        verbose_name=_("Module"), help_text=_("Module reference ie. pointer to the module.")
    )
    semester = models.ForeignKey(
        to="semesters.Semester", on_delete=models.CASCADE, related_name="exams",
        verbose_name=_("Semester"), help_text=_("Semester ref."),
    )
    write_date = models.DateField(
        verbose_name=_("Write date"), help_text=_("Date on which this exam is to be written")
    )
    write_time = models.TimeField(
        verbose_name=_("Write time"), help_text=_("Time on which this exam is to be written")
    )
    points = models.IntegerField(
        verbose_name=_("Points"), default=100,
        help_text=_("Indicates the exam total mark.")
    )

    class Meta:
        verbose_name = _("Exam")
        verbose_name_plural = _("Exams")
        ordering = ("-write_date", "-write_time")
        unique_together = ("module", "semester",)

    def __str__(self):
        return f"{str(self.module.name)}"

    def add_exam_to_students(self):
        from isppme.enrollments.models import RegisteredModules
        for reg in RegisteredModules.objects.filter(module=self.module, registration_record__semester=self.semester):
            RegistrationExam(
                exam=self,
                registration=reg.registration_record,
            ).save()
        return

    @property
    def write_datetime(self):
        d = datetime.datetime.combine(self.write_date, self.write_time)
        return d.astimezone(timezone.now().tzinfo)

    def save(self, *args, **kwargs):
        new_exam = False
        if not self.pk:
            new_exam = True
        super().save(*args, **kwargs)
        if new_exam:
            self.add_exam_to_students()

    @property
    def written(self):
        return self.write_datetime < timezone.now()


class RegistrationExam(UUIDModel):
    """Stores exams written or to be written by a student as per
    registration record.
    """

    exam = models.ForeignKey(
        to=Exam, on_delete=models.CASCADE, related_name="students",
        verbose_name=_("Exam"), help_text=_("Exam reference"), editable=False
    )
    registration = models.ForeignKey(
        to='enrollments.Registration', on_delete=models.CASCADE, related_name="exams",
        verbose_name=_("Registration"), help_text=_("Student registration reference"), editable=False
    )
    score = models.PositiveSmallIntegerField(
        verbose_name=_("Score"), default=0,
        help_text=_("Indicates the points scored by a student out of the exam total.")

    )

    class Meta:
        verbose_name = _("Student exam")
        verbose_name_plural = _("Student exams")
        unique_together = (
            ("exam", "registration"),
        )

    def __str__(self):
        return f"{str(self.exam.module.name)}"

    def clean(self):
        if self.score:
            if self.score > self.exam.points:
                raise ValidationError(
                    _("Score can not be greater than exam total.")
                )

    def save(self, *args, **kwargs):
        self.full_clean()
        super(RegistrationExam, self).save(*args, **kwargs)

    @property
    def student(self):
        st = self.registration.enrollment.student
        return f"{st.reg_number} - {st.full_name}"

    @property
    def exam_alias(self):
        return f"{self.exam.module.__str__()}"
