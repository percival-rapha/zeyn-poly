from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from .views import (
    login_view, create_teacher_account, logout_view, password_change_view,
    dashboard_view,
    # Modules
    module_list_view, module_assignment_list,

    # assignments
    create_assignment_view, assignment_list_view, assignment_detail_view, attach_assignment_files_view,
    edit_assignment_view,

    # submissions
    assignment_submission_list, submission_details_view, submit_review_view,

    # chat
    get_chat_url_view, chat_index_view, add_study_material_view,
)

urlpatterns = [
                  path(r'', login_view, name="login"),
                  path(r'logout/', logout_view, name="logout"),
                  path(r'create-account/', create_teacher_account, name="create_account"),
                  path(r'settings/change-password', password_change_view, name="change_password"),

                  # Dashboard
                  path(r"dashboard/", dashboard_view, name="dashboard"),

                  # modules
                  path(r'modules/', module_list_view, name="module_list"),
                  path(r'modules/<uuid:module_uuid>/extra-study-material/add/', add_study_material_view, name="add_module_material"),

                  # module assignments
                  path(r'modules/<uuid:module_uuid>/assignments/',
                       module_assignment_list, name="module_assignment_list"),

                  # assignments
                  path(r'assignments/', assignment_list_view, name="assignment_list"),
                  path(r'assignments/<uuid:assignment_uuid>/', assignment_detail_view, name="assignment_details"),
                  path(r'assignments/<uuid:assignment_uuid>/edit/', edit_assignment_view, name="edit_assignment"),
                  path(r'assignments/<uuid:assignment_uuid>/attach-files/',
                       attach_assignment_files_view, name="attach_assignment_files"),
                  path(r'modules/<uuid:module_uuid>/create-assignment/',
                       create_assignment_view, name="create_assignment"),

                  # submissions
                  path(r'assignments/<uuid:assignment_uuid>/submissions/',
                       assignment_submission_list, name="assignment_submission_list"),
                  path(r'submissions/<uuid:submission_uuid>/', submission_details_view, name="submission_details"),
                  path(r'submissions/<uuid:submission_uuid>/submit-review',
                       submit_review_view, name="submit_review"),

                  # Chatter
                  path('chat-contacts/', chat_index_view, name="chat_contacts"),
                  path('chat-contacts/<int:target_user>/', get_chat_url_view, name="get_chat_url"),

                  path('chat/', include('django_chatter.urls')),

                  # markdownx
                  path(r'markdownx/', include('markdownx.urls')),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if "debug_toolbar" in settings.INSTALLED_APPS and settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
