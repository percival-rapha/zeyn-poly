from django.apps import AppConfig


class StaffConfig(AppConfig):
    name = 'isppme.staff'
