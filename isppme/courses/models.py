import html

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import ValidationError, MinValueValidator

from isppme.core.models import UUIDModel

from markdownx.models import MarkdownxField
from markdownx.utils import markdownify


class Award(UUIDModel):
    """Stores award(s)
    e.g Diploma, Certificate, Foundation Certificate
    etc.
    """

    name = models.CharField(
        verbose_name=_("Award name"), max_length=100, unique=True,
        help_text=_("Award name e.g Certificate, Diploma, etc.")
    )

    registration_fee = models.DecimalField(
        verbose_name=_("Registration fee"), max_digits=19, decimal_places=2,
        help_text=_("Course award registration fee. e.g 20 for US $20")
    )

    late_registration_fee = models.DecimalField(
        verbose_name=_("Late registration fee"), max_digits=19, decimal_places=2, default=20.00,
        help_text=_("Late registration fee. e.g 20 for US $20")
    )
    module_fee = models.DecimalField(
        verbose_name=_("Module fee"), default=20, max_digits=19, decimal_places=2,
        help_text=_("Module fees for learning material"),
    )
    exam_fee = models.DecimalField(
        verbose_name=_("Exam fee"), default=20, max_digits=19, decimal_places=2,
        help_text=_("Exam fee to be paid before writing exams.")
    )

    class Meta:
        verbose_name = _("Course award")
        verbose_name_plural = _("Course awards")

    def __str__(self):
        return f"{str(self.name)}"


class CourseName(UUIDModel):
    """Stores course names
    e.g Project Planning, Event Planning, etc.
    """

    name = models.CharField(
        verbose_name=_("Course name"), max_length=255,
        help_text=_("Course name e.g Project Planning, dont include award type ie. diploma/certificate.")
    )

    about = MarkdownxField(
        null=True, blank=True, verbose_name=_("About")
    )

    class Meta:
        verbose_name = _("Course name")
        verbose_name_plural = _("Course names")

    def __str__(self):
        return f"{str(self.name)}"

    def qualifications_available(self):
        q = list()  # Qualifications available
        for cq in self.courses.all():
            q.append(cq.award.name)
        return ", ".join(q)

    @property
    def about_markdownified(self):
        return markdownify(html.escape(self.about))

class Course(UUIDModel):
    """Stores course names with their
    award types. e.g Foundation Certificate in Project Planning,
    Diploma in Event Management, etc."""

    award = models.ForeignKey(
        to="courses.Award", on_delete=models.CASCADE, related_name="courses",
        verbose_name=_("Course award"),
        help_text=_("This is a reference to the award type e.g. Certificate, Diploma etc.")
    )
    course_name = models.ForeignKey(
        to="courses.CourseName", on_delete=models.CASCADE, related_name="courses",
        verbose_name=_("Course name"), help_text=_("This is a reference to the course name.")
    )
    duration = models.PositiveSmallIntegerField(
        verbose_name=_("Course duration"), default=6,
        help_text=_("Course duration, in months e.g. 6, for 6 months")
    )
    semesters = models.SmallIntegerField(
        verbose_name=_("Semesters"), default=1,
        help_text=_("Course duration in number of semester. A semester is 6 months long.")
    )

    requirements = MarkdownxField(
        null=True, blank=True, verbose_name=_("Requirements")
    )

    class Meta:
        verbose_name = _("Course")
        verbose_name_plural = _("Courses")

    def __str__(self):
        return f"{str(self.award.name)} in {str(self.course_name)}"

    @property
    def sem2duration(self):
        return self.semesters * 3

    @property
    def requirements_markdownified(self):
        return markdownify(html.escape(self.requirements))

class CourseModules(UUIDModel):
    """Stores modules done in a course per semester.
    """

    course = models.ForeignKey(
        to='courses.Course', on_delete=models.CASCADE, related_name="course_modules",
        verbose_name=_("Course"), help_text=_("Course reference.")
    )
    module = models.ForeignKey(
        to='modules.Module', on_delete=models.CASCADE, related_name="course_modules",
        verbose_name=_("Module"), help_text=_("Module reference")
    )
    compulsory = models.BooleanField(
        verbose_name=_("Compulsory"), default=True, help_text=_("Indicates if this module is compulsory or not.")
    )
    semester_no = models.PositiveSmallIntegerField(
        verbose_name=_("Semester number"), default=1,
        help_text=_("The semester number in which this module is studied.")
    )
    core = models.BooleanField(
        verbose_name=_("Core"), default=True,
        help_text=_("Indicates if this is a core module.")
    )

    class Meta:
        verbose_name = _("Course module")
        verbose_name_plural = _("Course module")
        unique_together = (
            ("course", "module", "semester_no"),
        )

    def clean(self):
        if self.course and self.semester_no:
            if self.semester_no > self.course.semesters:
                raise ValidationError(
                    _(f"Semester number out of range. This course has {self.course.semesters} semester(s)."))

    def save(self, *args, **kwargs):
        self.full_clean()
        super(CourseModules, self).save(*args, **kwargs)


class CourseSemesterSetup(UUIDModel):
    """Stores semester setup/structure of a course
    for a certain course. These setups include:
    No. of modules to be studied, in a case where """

    course = models.ForeignKey(
        to="courses.Course", on_delete=models.CASCADE, related_name="semester_setup",
        verbose_name=_("Course"), help_text=_("Course reference."),
    )

    semester_no = models.PositiveSmallIntegerField(
        verbose_name=_("Semester no."), default=1,
        help_text=_("Semester number"),
        validators=[
            MinValueValidator(1, message=_("Semester number must be greater than 0"))
            # TODO: add this validator to all fields like this.
        ]
    )

    no_of_modules = models.PositiveSmallIntegerField(
        verbose_name=_("Number of modules"), default=1,
        validators=[
            MinValueValidator(1, message=_("Number of modules must be greater than 0"))
        ]
    )

    fee = models.DecimalField(
        verbose_name=_("Fee"), max_digits=19, decimal_places=2,
    )

    class Meta:
        verbose_name = "course setup"
        verbose_name_plural = _("course setups for semesters")
        unique_together = (
            ("course", "semester_no"),
        )

    def clean(self):
        super().clean()  # TODO: Make all clean methods on models have this
        if self.course and self.semester_no:
            if self.semester_no > self.course.semesters:
                raise ValidationError(
                    _(f"Semester number out of range. This course has {self.course.semesters} semester(s).")
                )

        if self.no_of_modules:
            # Make sure number of modules is greater than compulsory modules
            # for that semester.
            if self.course.course_modules.filter(compulsory=True,
                                                 semester_no=self.semester_no).count() > self.no_of_modules:
                raise ValidationError(
                    _(
                        "Please make sure the number of modules exceed the number of compulsory modules for this semester number.")
                )

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    def get_modules_for_semester(self):
        return self.course.course_modules.filter(
            semester_no=self.semester_no,
        )
