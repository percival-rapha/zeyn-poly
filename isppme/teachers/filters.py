from django.utils.translation import ugettext_lazy as _

import django_filters

from isppme.classroom.models import Assignment


class AssignmentFilter(django_filters.FilterSet):
    """
    Assignment filter for tutor Portal
    """

    label = django_filters.CharFilter(
        label=_(u"Search label"), lookup_expr='icontains',
    )

    class Meta:
        model = Assignment
        fields = [
            "label",
        ]
