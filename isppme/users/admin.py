from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model

from isppme.users.forms import UserChangeForm, UserCreationForm

User = get_user_model()

class EmailRequiredMixin(object):
    def __init__(self, *args, **kwargs):
        super(EmailRequiredMixin, self).__init__(*args, **kwargs)
        # make user email field required
        self.fields['email'].required = True


class MyUserCreationForm(EmailRequiredMixin, UserCreationForm):
    pass


class MyUserChangeForm(EmailRequiredMixin, UserChangeForm):
    pass

@admin.register(User)
class EmailRequiredUserAdmin(auth_admin.UserAdmin):
    form = MyUserChangeForm
    add_form = MyUserCreationForm
    list_display = ["username", "name", "email", "is_staff", "is_superuser"]
    search_fields = ["name"]
    add_fieldsets = ((None, {
        'fields': ('username', 'email', 'password1', 'password2'), 
        'classes': ('wide',)
    }),)

# admin.site.unregister(User)
# admin.site.register(User, EmailRequiredUserAdmin)

# @admin.register(User)
# class UserAdmin(auth_admin.UserAdmin):

#     form = UserChangeForm
#     add_form = UserCreationForm
#     add_fieldsets = (
#     (None, {
#         'classes': ('wide',),
#         'fields': ('email', 'username', 'password1', 'password2',)
#     }),)
#     fieldsets = (("User", {"fields": ("name",)}),) + auth_admin.UserAdmin.fieldsets
#     list_display = ["username", "name", "email", "is_superuser"]
#     search_fields = ["name"]
