from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from .views import (
    # Index/Landing
    index_view,

    # application process
    applicant_details_view,
    upload_application_documents_view,
    complete_application_view, acknowledge_application_view,
    create_student_account_view,
)

urlpatterns = [
                  path(r'', index_view, name="index"),

                  path(r'<uuid:course_uuid>/<slug:course_slug>/', applicant_details_view, name="applicant_details"),

                  path('upload-documents/<uuid:application_uuid>/', upload_application_documents_view,
                       name="upload_application_documents"),

                  path('complete-application/<uuid:application_uuid>/',
                       complete_application_view, name="complete_application"),
                  path('acknowledge-application/<uuid:application_uuid>/', acknowledge_application_view,
                       name="acknowledge_application"),

                  path('create-account/<uuid:application_uuid>/', create_student_account_view,
                       name="create_student_account"),

                  # markdownx
                  path(r'markdownx/', include('markdownx.urls')),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if "debug_toolbar" in settings.INSTALLED_APPS and settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
