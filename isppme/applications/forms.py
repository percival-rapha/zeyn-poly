from .models import Application, Document
from isppme.users.forms import UserCreationForm
from captcha.widgets import ReCaptchaV3
from captcha.fields import ReCaptchaField
from crispy_forms.layout import Layout, Field, Fieldset, Div, HTML, ButtonHolder, Submit
from crispy_forms.helper import FormHelper
from django import forms
from django.contrib.auth import get_user_model

User = get_user_model()


class ApplicationForm(forms.ModelForm):
    # captcha = ReCaptchaField(
    #     widget=ReCaptchaV3
    # )

    class Meta:
        model = Application
        fields = [
            "course", "entry_type", "title",
            "first_name", "last_name", "sex",
            "date_of_birth", "place_of_birth",
            "marital_status", "national_id",
            "passport_number", "nationality",
            "country_of_permanent_residence",
            "disabilities", "contact_address",
            "telephone_number", "mobile_number",
            "email_address", "is_member",
            "member_id", "colleges_attended",
            "work_experience", "referees",
        ]
        widgets = {
            "course": forms.HiddenInput(),
            "entry_type": forms.Select(
                attrs={
                    "data-toggle": "select",
                }
            ),
            "title": forms.Select(
                attrs={
                    "data-toggle": "select",
                }
            ),
            "sex": forms.Select(
                attrs={
                    "data-toggle": "select",
                }
            ),
            "date_of_birth": forms.DateInput(
                attrs={
                    "data-toggle": "flatpickr",
                }
            ),
            "marital_status": forms.Select(
                attrs={
                    "data-toggle": "select"
                }
            ),
            "nationality": forms.Select(
                attrs={
                    "data-toggle": "select",
                }
            ),
            "country_of_permanent_residence": forms.Select(
                attrs={
                    "data-toggle": "select",
                }
            ),
            "disabilities": forms.Textarea(
                attrs={
                    "rows": 5,
                }
            ),
            "colleges_attended": forms.Textarea(
                attrs={
                    "rows": 7
                }
            ),
            "work_experience": forms.Textarea(
                attrs={
                    "rows": 7,
                }
            ),
            "referees": forms.Textarea(
                attrs={
                    "rows": 7
                }
            )
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Next Step'))


class DocumentForm(forms.ModelForm):
    """
    Form for uploading course application
    related documents.
    """

    class Meta:
        model = Document
        fields = ["application", "label", "file", ]
        widgets = {
            "application": forms.HiddenInput(),
            "label": forms.TextInput(
                attrs={
                    "class": "form-control",
                }
            ),
            "file": forms.FileInput(
                attrs={
                    "class": "form-control",
                }
            )
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Upload Document'))


class StudentCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ("username", "email", )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Create Account'))

    def clean_email(self):
        email = self.cleaned_data.get('email', None)
        if not email:
            self.add_error("email", "Please enter an email address")
        return email
