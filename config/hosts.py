from django.conf import settings
from django_hosts import patterns, host

port = None
if settings.USE_CUSTOM_PORT:
    port = settings.CUSTOM_PORT

host_patterns = patterns('',
                         host(r'adl', settings.ROOT_URLCONF, name='root',
                              port=port),
                         host(r'apply', 'isppme.applications.hosts_urls', name='applications',
                              port=port),
                         host(r'staff', 'isppme.staff.hosts_urls', name='staff',
                              port=port),
                         host(r'students', 'isppme.students.hosts_urls', name='students',
                              port=port),
                         host(r'secure', 'isppme.core.secure_hosts_urls', name='secure',
                              port=port),
                         host(r'teachers', 'isppme.teachers.hosts_urls', name='teachers',
                              port=port),
                         host(r'www', settings.ROOT_URLCONF, name="chat", port=port)
                         )
