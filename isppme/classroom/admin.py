from django.contrib import admin

from markdownx.admin import MarkdownxModelAdmin

from .models import Assignment, RegistrationAssignments, Discussion, DiscussionComment


@admin.register(Assignment)
class AssignmentAdmin(MarkdownxModelAdmin):
    list_display = ("label", "module", "exclude_in_ca", "points", "graded", "created_by", "created",)


@admin.register(RegistrationAssignments)
class RegistrationAssignmentsAdmin(admin.ModelAdmin):
    list_display = ("registration", "assignment", "score", "created", )

@admin.register(Discussion)
class DCAdmin(MarkdownxModelAdmin):
    list_display = ("module_ref", "title", "created_by", "created", )
