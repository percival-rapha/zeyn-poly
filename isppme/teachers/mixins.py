from django.contrib.auth.mixins import AccessMixin

from django_hosts.resolvers import reverse as hosts_reverse


class TeacherLoginRequiredMixin(AccessMixin):
    """Verify that the current user is authenticated and is tutor."""

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        else:
            if not request.user.is_teacher:
                return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

    def get_login_url(self):
        return hosts_reverse('login', host='teachers',)


class TeacherContextMixin:
    """Adds a/the logged in tutor's record
    in the context data var.
    """

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["teacher"] = self.request.user.teacher
        return context
