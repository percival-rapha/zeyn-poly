import html
from datetime import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.validators import ValidationError

from markdownx.models import MarkdownxField
from markdownx.utils import markdownify
from upload_validator import FileTypeValidator

from isppme.core.models import UUIDModel
from isppme.users.models import get_sentinel_user

class Assignment(UUIDModel):
    """Stores an assignment record"""

    label = models.CharField(
        verbose_name=_("Label"), max_length=255,
        help_text=_("Label or assignment title")
    )
    module = models.ForeignKey(
        to="modules.Module", on_delete=models.CASCADE, related_name="assignments",
        verbose_name=_("Module"), help_text=_("Module reference")
    )
    due_date = models.DateField(verbose_name=_("Due date"))
    due_time = models.TimeField(verbose_name=_("Due Time"))
    exclude_in_ca = models.BooleanField(
        verbose_name=_("Exclude in continuous assessment"), default=False,
        help_text=_("Indicates if an assignment is to be excluded in determining module mark total.")
    )
    description = MarkdownxField(
        verbose_name=_("Description"),
    )
    points = models.PositiveSmallIntegerField(
        verbose_name=_("Points"), default=100,
        help_text=_("Assignment total points")
    )
    graded = models.BooleanField(
        verbose_name=_("Graded"), default=False,
        help_text=_("Indicates if assignment has been graded yet.")
    )
    created_by = models.ForeignKey(
        to="teachers.Teacher", on_delete=models.SET_NULL, null=True, blank=True,
        related_name="assignments", verbose_name=_("Created by"),
        help_text=_("Ref. to tutor/lecturer who created the assignment")
    )

    class Meta:
        verbose_name = _("Assignment")
        verbose_name_plural = _("Assignments")

    def __str__(self):
        return f"{str(self.label)}"

    def add_assignment_to_students(self):
        from isppme.semesters.models import Semester
        from isppme.enrollments.models import RegisteredModules
        CURRENT_SEMESTER = Semester.objects.get_current_semester()
        if CURRENT_SEMESTER:
            if self.created.date() >= CURRENT_SEMESTER.start and self.created.date() <= CURRENT_SEMESTER.end:
                for reg in RegisteredModules.objects.filter(module=self.module, registration_record__semester=CURRENT_SEMESTER):
                    RegistrationAssignments.objects.create(
                        registration=reg.registration_record,
                        assignment=self,
                    )
        return

    def add_assignment_to_student(self, reg_record):
        try:
            RegistrationAssignments.objects.create(
                registration=reg_record,
                assignment=self,
            )
        except Exception as ex:
            pass
        return

    @property
    def title(self):
        return str(self.label)

    def clean(self):
        if self.due_date:
            if self.due_date < timezone.now().date():
                # Due date must be at least a day ahead.
                raise ValidationError(_("Assignment due date must be at least a day from now."))

    def save(self, *args, **kwargs):
        new_assignment = False
        if not self.pk:
            new_assignment = True
        self.full_clean()
        super(Assignment, self).save(*args, **kwargs)
        if new_assignment:
            self.add_assignment_to_students()

    @property
    def description_formatted_markdown(self):
        return markdownify(html.escape(self.description))

    @property
    def get_description(self):
        return html.escape(self.description)

    @property
    def due_date_time(self):
        return datetime.combine(self.due_date, self.due_time)

    @property
    def created_date(self):
        return self.created.date()

    @property
    def expired(self):
        if datetime.now() > self.due_date_time:
            return True
        return False

    @property
    def submitted_students(self, ):
        """Returns a queryset
        of all the students who
        have made submissions
        """
        return [s.student for s in self.submissions.all()]


class AssignmentDocument(UUIDModel):
    """Stores documents related/linked to
    assignments. These can be: instructions on an assignment,
    notes etc.
    """
    label = models.CharField(
        max_length=255, verbose_name=_("Label"), help_text="Document label"
    )
    assignment = models.ForeignKey(
        to="classroom.Assignment", on_delete=models.CASCADE, verbose_name=_("Assignment"),
        related_name="documents",
    )
    file = models.FileField(
        upload_to='assignments/%Y/%m/',
        validators=[
            FileTypeValidator(
                allowed_types=[
                    'image/jpeg',
                    'text/plain',
                    'image/png',
                    'application/pdf',
                    "application/msword",
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                    # TODO: add more document formats
                ]
            )]
    )

    class Meta:
        verbose_name = _("Assignment document")
        verbose_name_plural = _("Assignment documents")
        ordering = ("-created",)

    def __str__(self):
        return str(self.label)


class Submission(UUIDModel):
    """Repr(s) a submission made to an Assignment
    """
    assignment = models.ForeignKey(
        to="classroom.Assignment", on_delete=models.CASCADE, verbose_name=_("Assignment"),
        related_name="submissions", help_text=_("Assignment ref.")
    )
    student = models.ForeignKey(
        to="students.Student", on_delete=models.CASCADE, verbose_name=_("Student"),
        related_name="submissions", help_text=_("Student ref.")
    )
    write_up = MarkdownxField(
        verbose_name=_("Write-Up"), null=True, blank=True,
        help_text=_("Solution/Description or any other additional notes."),
    )
    graded = models.BooleanField(default=False, editable=False)

    class Meta:
        verbose_name = _("Assignment submission")
        verbose_name_plural = _("Assignment submissions")
        unique_together = (
            ("assignment", "student",)
        )
        ordering = ("-created", "-modified",)

    @property
    def write_up_formatted_markdown(self):
        return markdownify(html.escape(self.write_up))

    @property
    def get_write_up(self):
        return html.escape(self.write_up)

    def mark_as_graded(self):
        if hasattr(self, "review"):
            self.graded = True
            self.save()
        return

    @property
    def has_review(self):
        if hasattr(self, "review"):
            return True
        return False

    def save(self, *args, **kwargs):
        # Save
        super().save(*args, **kwargs)


class SubmissionDocument(UUIDModel):
    """Stores documents upload on assignment
    submission.
    """

    label = models.CharField(
        max_length=255, null=True, blank=True, verbose_name=_("Label"),
        help_text=_("Document label. If applicable")
    )
    submission = models.ForeignKey(
        to="classroom.Submission", on_delete=models.CASCADE, verbose_name=_("Submission"),
        related_name="documents", help_text=_("Submission ref."),
    )
    file = models.FileField(
        upload_to='submissions/%Y/%m/',
        validators=[
            FileTypeValidator(
                allowed_types=[
                    'image/jpeg',
                    'text/plain',
                    'image/png',
                    'application/pdf',
                    "application/msword",
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                ]
            )
        ]
    )

    class Meta:
        verbose_name = _("Assignment submission document")
        verbose_name_plural = _("Assignment submission documents")
        ordering = ("-created", "-modified",)



class Review(UUIDModel):
    """Stores review(s) on assignment
    solutions Submissions.
    """
    submission = models.OneToOneField(
        to=Submission, on_delete=models.CASCADE, related_name="review",
        verbose_name=_("Submission"), help_text=_("Submission reference"),
    )
    remarks = models.TextField(
        verbose_name=_("Remarks"), null=True, blank=True,
    )
    points = models.PositiveSmallIntegerField(verbose_name=_("Points"))

    class Meta:
        verbose_name = _("Assignment submission review")
        verbose_name_plural = _("Assignment submission reviews")

    def clean(self):
        super().clean()
        if self.submission and self.points:
            max_points = self.submission.assignment.points
            if self.points > max_points:
                raise ValidationError(_(f"Points cannot be greater than {max_points}"))

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)
        self.submission.mark_as_graded()



class RegistrationAssignments(UUIDModel):
    """Stores assignments for a student as per registration record."""

    registration = models.ForeignKey(
        to="enrollments.Registration", on_delete=models.CASCADE, related_name="assignments",
        verbose_name=_("Student"), help_text=_("Student reference"),
    )

    assignment = models.ForeignKey(
        to=Assignment, on_delete=models.CASCADE, related_name="students",
        verbose_name=_("Assignment"), help_text=_("Assignment reference"),
    )

    # score = models.IntegerField(
    #     verbose_name=_("Score"), default=0,
    #     help_text=_("Points scored by student in assignment")
    # )

    class Meta:
        verbose_name = _("Student assignment")
        verbose_name_plural = _("Student assignments")
        unique_together = ("registration", "assignment")

    def __str__(self):
        return f"{str(self.assignment.label)}"

    @property
    def score(self):
        sub = self.assignment.submissions.filter(
            student=self.registration.enrollment.student
        )
        if sub.count() > 0:
            sub = sub[0]
            if sub.has_review:
                return sub.review.points
            return 0
        return 0




class Discussion(UUIDModel):
    """Stores discussions on topics in modules"""

    module_ref = models.ForeignKey(
        to="modules.Module", on_delete=models.CASCADE, related_name="discussions",
        verbose_name=_("Module")
    )
    title = models.CharField(
        verbose_name=_("Title"), max_length=255,
        help_text=_("Discussion title")
    )

    created_by = models.ForeignKey(
        to="students.Student", on_delete=models.CASCADE, related_name="discussions",
        verbose_name=_("Created by"),
    )

    details = MarkdownxField(
        verbose_name=_("Details")
    )

    closed = models.BooleanField(default=False)

    class Meta:
        verbose_name = _("discussion")
        verbose_name_plural = _("discussions")
        ordering = ("-created", )

    @property
    def get_details_markdownified(self):
        return markdownify(html.escape(self.details))


class DiscussionComment(UUIDModel):
    """Comments to a discussion"""

    student = models.ForeignKey(
        to='students.Student', on_delete=models.CASCADE, related_name="comments",
        verbose_name=_("Student")
    )
    discussion = models.ForeignKey(
        to="classroom.Discussion", on_delete=models.CASCADE, related_name="comments",
        verbose_name=_("Discussion")
    )
    details = MarkdownxField(
        verbose_name=_("Details")
    )

    class Meta:
        verbose_name_plural = _("Discussion comments")
        ordering = ("-created", "-modified", )

    @property
    def get_details_markdownified(self):
        return markdownify(html.escape(self.details))

    def save(self, *args, **kwargs):
        if self.discussion.closed:
            # Disable comments on closed discussions
            return
        super().save(*args, **kwargs)
