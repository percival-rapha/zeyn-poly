from django.contrib import admin

from markdownx.admin import MarkdownxModelAdmin

from .models import Event


@admin.register(Event)
class EAdmin(MarkdownxModelAdmin):
    list_display = ["name", "start_date", "start_time", "venue", "created", ]
