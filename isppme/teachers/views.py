from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.views import generic, View
from django.contrib.auth import views as auth_views, logout, get_user_model
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib import messages
from django.db import IntegrityError, transaction
from django.http import HttpResponseRedirect, HttpResponse

from django_hosts.resolvers import reverse as hosts_reverse
from django_filters.views import FilterView
from django_chatter.utils import create_room

from isppme.classroom.models import Assignment, Submission
from isppme.modules.models import Module, MaterialUploads

from .forms import (
    TeacherAuthenticationForm, CreateTeacherAccountForm, CustomPasswordChangeForm,
    AssignmentForm, AssignmentDocumentForm, ReviewForm, StudyMaterialForm
)
from .mixins import TeacherLoginRequiredMixin, TeacherContextMixin
from .filters import AssignmentFilter


class BaseTeacherView(TeacherLoginRequiredMixin, TeacherContextMixin):
    pass


class Login(auth_views.LoginView):
    template_name = "teachers/login.html"
    authentication_form = TeacherAuthenticationForm

    def get_success_url(self):
        return hosts_reverse("dashboard", host="teachers", )


login_view = Login.as_view()


class Logout(TeacherLoginRequiredMixin, View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(
            self.get_login_url()
        )


logout_view = Logout.as_view()


class CreateAccountView(generic.CreateView):
    template_name = "teachers/create_account.html"
    form_class = CreateTeacherAccountForm

    def form_valid(self, form):
        try:
            with transaction.atomic():
                self.object = form.save()
                user = self.object
                if not user.is_teacher:
                    form.teacher_record.user = user
                    form.teacher_record.save()
        except IntegrityError:
            return "Error"  # TODO: Create Actual Error

        return HttpResponseRedirect("/")


create_teacher_account = CreateAccountView.as_view()


class Dashboard(TeacherLoginRequiredMixin, TeacherContextMixin, generic.TemplateView):
    template_name = "teachers/dashboard.html"


dashboard_view = Dashboard.as_view()


class ChangePasswordView(TeacherLoginRequiredMixin, TeacherContextMixin, auth_views.PasswordChangeView):
    template_name = "teachers/change_password.html"
    form_class = CustomPasswordChangeForm

    def get_success_url(self):
        return hosts_reverse("login", host="teachers")


password_change_view = ChangePasswordView.as_view()


class ModuleListView(TeacherLoginRequiredMixin, TeacherContextMixin, generic.ListView):
    template_name = "teachers/module_list.html"
    context_object_name = "modules"

    # model = Module

    def get_queryset(self):
        teacher = self.request.user.teacher
        return teacher.modules.all()


module_list_view = ModuleListView.as_view()


class CreateAssignmentView(TeacherLoginRequiredMixin, TeacherContextMixin, generic.CreateView):
    template_name = "teachers/create_assignment.html"
    form_class = AssignmentForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user.teacher
        return super().form_valid(form)

    def get_success_url(self):
        return hosts_reverse("assignment_details", kwargs={"assignment_uuid": self.object.uuid_id}, host="teachers")

    def dispatch(self, request, *args, **kwargs):
        self.module = get_object_or_404(Module, uuid_id=kwargs.get("module_uuid", ""))
        self.initial.update(
            {"module": self.module}
        )

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["module"] = self.module
        return context


create_assignment_view = CreateAssignmentView.as_view()


class Assignments(TeacherLoginRequiredMixin, TeacherContextMixin, FilterView):
    template_name = "teachers/assignment_list.html"
    context_object_name = "assignments"
    filterset_class = AssignmentFilter
    paginate_by = 20

    def get_queryset(self):
        return self.request.user.teacher.assignments.all()


assignment_list_view = Assignments.as_view()


class AssignmentDetailView(TeacherLoginRequiredMixin, TeacherContextMixin, generic.DetailView):
    """View to display assignment Details
    """

    template_name = "teachers/assignment_details.html"
    model = Assignment
    slug_url_kwarg = "assignment_uuid"
    slug_field = "uuid_id"
    context_object_name = "assignment"


assignment_detail_view = AssignmentDetailView.as_view()


class AttachAssignmentFiles(TeacherLoginRequiredMixin, TeacherContextMixin, generic.CreateView):
    """View to add files to an already created assignment.
    Files might be images, pdf or word documents (doc(x), txt, etc)
    """
    form_class = AssignmentDocumentForm
    template_name = "teachers/attach_assignment_files.html"

    def get_success_url(self):
        return hosts_reverse("assignment_details", kwargs={"assignment_uuid": self.object.assignment.uuid_id},
                             host="teachers")

    def dispatch(self, request, *args, **kwargs):
        self.assignment = get_object_or_404(Assignment, uuid_id=kwargs["assignment_uuid"])
        self.initial.update(
            {"assignment": self.assignment}
        )
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["assignment"] = self.assignment
        return context


attach_assignment_files_view = AttachAssignmentFiles.as_view()


class EditAssignmentView(TeacherLoginRequiredMixin, UserPassesTestMixin, TeacherContextMixin, generic.UpdateView):
    form_class = AssignmentForm
    model = Assignment
    template_name = "teachers/edit_assignment.html"
    slug_field = "uuid_id"
    slug_url_kwarg = "assignment_uuid"
    context_object_name = "assignment"

    def test_func(self):
        return self.request.user.teacher == self.get_object().created_by

    def get_success_url(self):
        return hosts_reverse("assignment_details", kwargs={"assignment_uuid": self.object.uuid_id}, host="teachers")


edit_assignment_view = EditAssignmentView.as_view()


class ModuleAssignmentList(TeacherLoginRequiredMixin, TeacherContextMixin, FilterView):
    template_name = "teachers/module_assignment_list.html"
    context_object_name = "assignments"

    filterset_class = AssignmentFilter

    def get_queryset(self):
        return self.request.user.teacher.assignments.filter(module=self.module)

    def dispatch(self, request, *args, **kwargs):
        self.module = get_object_or_404(Module, uuid_id=kwargs["module_uuid"])
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["module"] = self.module
        return context


module_assignment_list = ModuleAssignmentList.as_view()


class AssignmentSubmissionList(TeacherLoginRequiredMixin, TeacherContextMixin, generic.ListView):
    template_name = "teachers/assignment_submission_list.html"
    context_object_name = "submissions"

    def dispatch(self, request, *args, **kwargs):
        self.assignment = get_object_or_404(Assignment, uuid_id=kwargs["assignment_uuid"])
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.assignment.submissions.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["assignment"] = self.assignment
        return context


assignment_submission_list = AssignmentSubmissionList.as_view()


class SubmissionDetails(TeacherLoginRequiredMixin, TeacherContextMixin, generic.DetailView):
    model = Submission
    template_name = "teachers/submission_details.html"
    context_object_name = "submission"
    slug_url_kwarg = "submission_uuid"
    slug_field = "uuid_id"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        review_form = None
        if hasattr(self.get_object(), "review"):
            review_form = ReviewForm(instance=self.get_object().review)
        else:
            review_form = ReviewForm(
                initial={"submission": self.get_object()}
            )
        context["review_form"] = review_form
        return context


submission_details_view = SubmissionDetails.as_view()


class SubmitReview(TeacherLoginRequiredMixin, TeacherContextMixin, View):
    def post(self, request, *args, **kwargs):
        submission = get_object_or_404(Submission, uuid_id=kwargs["submission_uuid"])
        updated = False
        form = ReviewForm(request.POST)
        if submission.has_review:
            updated = True
            form = ReviewForm(request.POST, instance=submission.review)

        if form.is_valid():
            form.save()
            if updated:
                messages.success(request, "Review successfully updated.")
            else:
                messages.success(request, "Review successfully set for this submission.")
        else:
            if form.non_field_errors():
                for error in form.non_field_errors():
                    messages.warning(request, error)

            for field in form.fields:
                if hasattr(field, "errors"):
                    messages.warning(request, field.errors[0])

        return HttpResponseRedirect(
            hosts_reverse("submission_details", kwargs={
                "submission_uuid": submission.uuid_id}, host="teachers")
        )


submit_review_view = SubmitReview.as_view()


####################################################################################################
# Chat                                                                                             #
# Chat views for students                                                                          #
# Written by Percival Rapha (c) ISPPME 2019                                                        #
####################################################################################################


class ChatIndex(BaseTeacherView, generic.TemplateView):
    template_name = "teachers/chat_contacts.html"


chat_index_view = ChatIndex.as_view()


class GetChatUrl(BaseTeacherView, View):
    def get(self, request, *args, **kwargs):
        user = get_user_model().objects.get(username=request.user)
        target_user = get_user_model().objects.get(pk=kwargs["target_user"])

        '''
        AI-------------------------------------------------------------------
            Use the util room creation function to create room for one/two
            user(s). This can be extended in the future to add multiple users
            in a group chat.
        -------------------------------------------------------------------AI
        '''
        if (user == target_user):
            room_id = create_room([user])
        else:
            room_id = create_room([user, target_user])
        return HttpResponseRedirect(
            reverse('django_chatter:chatroom', args=[room_id])
        )


get_chat_url_view = GetChatUrl.as_view()


class AddStudyMaterial(BaseTeacherView, generic.CreateView):
    template_name = "teachers/add_study_material.html"
    form_class = StudyMaterialForm

    def __init__(self, **kwargs):
        self.module_ref = None
        super().__init__(**kwargs)

    def dispatch(self, request, *args, **kwargs):
        self.module_ref = get_object_or_404(Module, uuid_id=kwargs["module_uuid"])
        self.initial.update({"module_ref": self.module_ref})
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return hosts_reverse("module_list", host="teachers")

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["module"] = self.module_ref
        return ctx



add_study_material_view = AddStudyMaterial.as_view()
