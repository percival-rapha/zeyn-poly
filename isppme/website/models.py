import html

from django.db import models
from django.utils.translation import ugettext_lazy as _

from markdownx.models import MarkdownxField
from markdownx.utils import markdownify

from isppme.core.models import UUIDModel


class DownloadCategory(UUIDModel):
    name = models.CharField(
        max_length=255,
    )

    class Meta:
        verbose_name = _("DL Category")
        verbose_name_plural = _("DL Categories")


class Download(UUIDModel):
    category = models.ForeignKey(
        to=DownloadCategory, on_delete=models.CASCADE, related_name="downloads",
    )
    name = models.CharField(
        max_length=255, verbose_name=_("Name")
    )

    file = models.FileField(
        upload_to='downloads/%Y/%m/',
    )
    details = MarkdownxField(
        null=True, blank=True,
    )

    class Meta:
        verbose_name = _("Download")
        verbose_name_plural = _("Downloads")

    @property
    def details_markdownified(self):
        return markdownify(html.escape(self.details))
