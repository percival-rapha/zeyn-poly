from django.contrib import admin

from .models import Exam, RegistrationExam


@admin.register(Exam)
class ExamAdmin(admin.ModelAdmin):
    list_display = ("module", "semester", "write_date", "write_time", "points",)


@admin.register(RegistrationExam)
class RegistrationExamAdmin(admin.ModelAdmin):
    list_display = ("student", "exam_alias", "score",)
    search_fields = ('registration__enrollment__student__name', 'registration__enrollment__student__reg_number', 'exam__module__name', 'exam__module__code')
    list_filter = ("exam__semester", "exam__module")
