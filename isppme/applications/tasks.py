from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context

import celery
from django_hosts.resolvers import reverse_host_lazy, reverse_lazy

from .models import Application


@celery.task(name="tasks.send_application_successful_email")
def send_email(application_id):
    # Get application
    application = Application.objects.get(id=application_id)
    # application.complete()

    subject = "Bulawayo Polytechnic Application Successful"
    from_email = "Bulawayo Polytechnic Admissions <applications@byopoly.com>"
    to_email = application.email_address

    plain_text = get_template("email/applications/application_successful.txt")
    html_template = get_template("email/applications/application_successful.html")
    context_data = {
        "name": application.full_name,
        "course": application.course,
    }

    text_content = plain_text.render(context_data)
    html_content = html_template.render(context_data)

    msg = EmailMultiAlternatives(subject, text_content, from_email, [to_email])
    # msg.attach_alternative(html_content, "text/html")
    msg.send()


@celery.task(name="tasks.send_application_accepted_email")
def notify_applicant(application_id):
    try:
        application = Application.objects.get(id=application_id)
    except Exception as ex:
        # TODO: Log Exception
        pass
    else:
        # Send email
        subject = "Your Bulawayo Polytechnic Application"
        from_email = "Bulawayo Polytechnic Admissions <applications@byopoly.ac.zw>"
        to_email = application.email_address

        plain_text = get_template("email/applications/application_accepted.txt")
        html_template = get_template("email/applications/application_accepted.html")
        context_data = {
            "application": application,
            "url": reverse_lazy("acknowledge_application", kwargs={"application_uuid": application.uuid_id}, host="applications", scheme="http"),
            "link": "jndd",
        }

        text_content = plain_text.render(context_data)
        html_content = html_template.render(context_data)

        msg = EmailMultiAlternatives(subject, text_content, from_email, [to_email])
        msg.attach_alternative(html_content, "text/html")
        msg.send()

