from django.contrib.auth.mixins import AccessMixin

from django_hosts.resolvers import reverse as hosts_reverse

from isppme.semesters.models import CURRENT_SEMESTER


class TeacherOrStudentLoginRequired(AccessMixin):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        else:
            if not request.user.is_student or request.user.is_teacher:
                return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

    def get_login_url(self):
        return hosts_reverse('login', host='students',)
