from django.contrib import admin

from .models import Module, LearningMaterial

@admin.register(Module)
class ModuleAdmin(admin.ModelAdmin):
    list_display = ("code", "name", "continuous_assessment_contribution", "exam_contribution", )

@admin.register(LearningMaterial)
class LMAdmin(admin.ModelAdmin):
    list_display = ("label", "module_ref", "file", "about", "created", "modified", )
