from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.views import View, generic
from django.contrib.auth import (views as auth_views, mixins as auth_mixins, logout)
from django.contrib import messages
from django.conf import settings

from django_hosts.resolvers import reverse as hosts_reverse, reverse_lazy as hosts_reverse_lazy
from django_filters.views import FilterView

from isppme.applications.models import Application, Document
from isppme.applications.forms import ApplicationForm
# from isppme.enrollment.models import Enrollment
from isppme.students.models import Student
# from isppme.accounts.models import Transaction, RegistrationPayment, Ledger
from isppme.teachers.models import Teacher
from isppme.teachers.forms import AddTutorForm
from isppme.courses.models import Course
from isppme.modules.models import Module
from isppme.semesters.models import Semester
from isppme.exams.models import Exam

from .forms import (
    StaffAuthenticationForm, AcceptanceForm, CustomPasswordChangeForm, ExamForm,
)
from .mixins import StaffLoginRequiredMixin, StaffContextMixin
from .filters import (
    StudentFilter, ApplicationFilter, TeacherFilter, ModuleFilter
)


class BaseStaffView(StaffLoginRequiredMixin, StaffContextMixin):
    pass


class Login(auth_views.LoginView):
    authentication_form = StaffAuthenticationForm
    template_name = "staff/login.html"

    def get_success_url(self):
        return hosts_reverse("dashboard", host="staff", )


login_view = Login.as_view()


class Logout(StaffLoginRequiredMixin, View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(
            self.get_login_url()
        )


logout_view = Logout.as_view()


class Dashboard(StaffLoginRequiredMixin, generic.TemplateView):
    template_name = "staff/dashboard.html"


dashboard_view = Dashboard.as_view()


class Applications(StaffLoginRequiredMixin, FilterView):
    template_name = "staff/application_list.html"
    model = Application
    paginate_by = 20
    filterset_class = ApplicationFilter


application_listings_view = Applications.as_view()


class PDFViewer(StaffLoginRequiredMixin, generic.TemplateView):
    template_name = "staff/pdf_embed.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Get Document
        document = get_object_or_404(Document, uuid_id=kwargs['document_uuid'])
        context.update({"file_name": document.label, "url": document.file.url})
        return context


pdf_viewer_view = PDFViewer.as_view()


class SingleApplicationView(StaffLoginRequiredMixin, generic.DetailView):
    template_name = "staff/application.html"
    model = Application
    context_object_name = "application"
    slug_field = 'uuid_id'
    slug_url_kwarg = 'application_uuid'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                'acceptance_form': AcceptanceForm(initial={"application": self.get_object().id})
            }
        )
        return context


single_application_view = SingleApplicationView.as_view()


class AcceptApplication(StaffLoginRequiredMixin, View):
    def __init__(self, **kwargs):
        self.application = None
        super().__init__(**kwargs)

    def dispatch(self, request, *args, **kwargs):
        self.application = get_object_or_404(Application, uuid_id=kwargs["application_uuid"])
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        redirect_to = None
        if "next" in request.GET:
            redirect_to = request.GET.get("next")
        else:
            redirect_to = hosts_reverse_lazy(
                "single_application", kwargs={"application_uuid": self.application.uuid_id}, host="staff"
            )

        form = AcceptanceForm(request.POST)
        if form.is_valid():
            self.application.accept(form.cleaned_data.get("semester"))
            # TODO: Add message notifying success
            return HttpResponseRedirect(redirect_to)
        else:
            # TODO: Add message, warning user of errors
            return HttpResponseRedirect(redirect_to)


accept_application_view = AcceptApplication.as_view()


class RejectApplication(StaffLoginRequiredMixin, View):
    def get(self, request, **kwargs):
        application = get_object_or_404(Application, uuid_id=kwargs["application_uuid"])
        if application.rejected:
            # TODO: Add contrib.messages to notify user that
            # application has already been rejected
            pass
        else:
            application.reject()
            # TODO: Add contrib.messages to notify user that
            # application has been accepted successfully
        # TODO: Send email to applicant after 10 minutes

        # Redirect Back
        redirect_to = None
        if "next" in request.GET:
            redirect_to = request.GET.get("next")
        else:
            redirect_to = hosts_reverse_lazy(
                "single_application", kwargs={"application_uuid": application.uuid_id}, host="staff"
            )
        return HttpResponseRedirect(redirect_to)


reject_application_view = RejectApplication.as_view()




class StudentsView(StaffLoginRequiredMixin, FilterView):
    filterset_class = StudentFilter
    model = Student
    template_name = "staff/student_list.html"
    paginate_by = 20


student_list_view = StudentsView.as_view()


class SingleStudentView(StaffLoginRequiredMixin, generic.DetailView):
    model = Student
    context_object_name = "student"
    template_name = "staff/student_detail.html"
    slug_url_kwarg = "student_uuid"
    slug_field = "uuid_id"


single_student_view = SingleStudentView.as_view()


# class EnrollmentsView(StaffLoginRequiredMixin, FilterView):
#     """
#     Lists enrollments
#     """
#     template_name = "staff/enrollments_list.html"
#     model = Enrollment
#     context_object_name = "enrollments"
#     paginate_by = 20
#     filterset_class = EnrollmentFilter
#
#
# enrollments_list_view = EnrollmentsView.as_view()
#
#
# class TransactionsView(StaffLoginRequiredMixin, FilterView):
#     template_name = "staff/transaction_list.html"
#     filterset_class = TransactionFilter
#     model = Transaction
#     paginate_by = 20
#     context_object_name = "transactions"
#
#
# transaction_list_view = TransactionsView.as_view()
#
#
# class RegPaymentsView(StaffLoginRequiredMixin, FilterView):
#     template_name = 'staff/registration_payment_list.html'
#     filterset_class = RegPaymentsFilter
#     paginate_by = 20
#     model = RegistrationPayment
#
#
# registration_payments_view = RegPaymentsView.as_view()
#
#
# class StudentAccountsView(StaffLoginRequiredMixin, FilterView):
#     template_name = "staff/student_accounts.html"
#     model = Ledger
#     paginate_by = 30
#     filterset_class = LedgerFilter
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context["total_owing"] = Ledger.objects.total_owing
#         context["total_paid"] = Ledger.objects.total_credits
#         context["number_of_accounts"] = Student.objects.count()
#         return context
#
#
# student_accounts_view = StudentAccountsView.as_view()
#
#
class CreateTeacherView(StaffLoginRequiredMixin, generic.CreateView):
    template_name = "staff/create_teacher.html"
    form_class = AddTutorForm

    def get_success_url(self):
        return hosts_reverse("teacher_detail", kwargs={"teacher_uuid": self.object.uuid_id}, host="staff")


create_teacher_view = CreateTeacherView.as_view()


class TeacherListingView(StaffLoginRequiredMixin, FilterView):
    template_name = "staff/teacher_list.html"
    model = Teacher
    context_object_name = "teachers"
    paginate_by = 20
    filterset_class = TeacherFilter


teacher_list_view = TeacherListingView.as_view()


class SingleTeacherView(StaffLoginRequiredMixin, generic.DetailView):
    model = Teacher
    context_object_name = "teacher"
    slug_url_kwarg = "teacher_uuid"
    slug_field = "uuid_id"
    template_name = "staff/teacher_detail.html"


teacher_detail_view = SingleTeacherView.as_view()


class EditTeacherView(BaseStaffView, generic.UpdateView):
    model = Teacher
    context_object_name = "teacher"
    slug_url_kwarg = "teacher_uuid"
    slug_field = "uuid_id"
    template_name = "staff/update_teacher.html"
    form_class = AddTutorForm

    def get_success_url(self):
        return hosts_reverse("teacher_detail", kwargs={"teacher_uuid": self.object.uuid_id}, host="staff")


edit_teacher_view = EditTeacherView.as_view()


class ChangePasswordView(BaseStaffView, auth_views.PasswordChangeView):
    template_name = "staff/change_password.html"
    form_class = CustomPasswordChangeForm

    def get_success_url(self):
        return hosts_reverse("login", host="staff")


password_change_view = ChangePasswordView.as_view()


class CreateExamView(BaseStaffView, generic.CreateView):
    form_class = ExamForm
    template_name = "staff/create_exam.html"

    def __init__(self, **kwargs):
        self.module_ref = None
        super().__init__(**kwargs)

    def dispatch(self, request, *args, **kwargs):
        self.module_ref = get_object_or_404(Module, uuid_id=kwargs["module_uuid"])
        CURRENT_SEMESTER = Semester.objects.get_current_semester()

        self.initial.update(
            {"module": self.module_ref, "semester": CURRENT_SEMESTER}
        )
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return hosts_reverse("exam_list", host="staff")

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["module"] = self.module_ref
        return ctx


create_exam_view = CreateExamView.as_view()


class EditExamView(BaseStaffView, generic.UpdateView):
    form_class = ExamForm
    model = Exam
    context_object_name = "exam"
    slug_url_kwarg = "exam_uuid"
    slug_field = "uuid_id"
    template_name = "staff/edit_exam.html"

    def get_success_url(self):
        return hosts_reverse("exam_list", host="staff")

edit_exam_view = EditExamView.as_view()


class ExamListView(BaseStaffView, generic.ListView):
    template_name = "staff/exam_list.html"
    context_object_name = "exams"

    def get_queryset(self):
        CURRENT_SEMESTER = Semester.objects.get_current_semester()

        if CURRENT_SEMESTER:
            return CURRENT_SEMESTER.exams.all()
        return None


exam_list_view = ExamListView.as_view()


class ExamCalendar(ExamListView):
    template_name = "staff/exam_calendar.html"


exam_calendar_view = ExamCalendar.as_view()


class ModuleListView(BaseStaffView, FilterView):
    template_name = "staff/module_list.html"
    paginate_by = 20
    context_object_name = "modules"
    filterset_class = ModuleFilter


module_list_view = ModuleListView.as_view()


class CourseListView(BaseStaffView, generic.ListView):
    model = Course
    template_name = "staff/course_list.html"
    context_object_name = "courses"


course_list_view = CourseListView.as_view()


class CourseDetailView(BaseStaffView, generic.DetailView):
    model = Course
    template_name = "staff/course_detail.html"
    context_object_name = "course"
    slug_field = "uuid_id"
    slug_url_kwarg = "course_uuid"


course_detail_view = CourseDetailView.as_view()
