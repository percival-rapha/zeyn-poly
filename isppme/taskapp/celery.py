from __future__ import absolute_import, unicode_literals
import os
import environ

from django.apps import apps, AppConfig
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured

from celery import Celery

env = environ.Env()

# https://github.com/twoscoops/django-twoscoops-project/blob/develop/project_name/project_name/settings/production.py#L14-L21
def get_env_setting(setting):
    """ Get the environment setting or return exception """
    try:
        return os.environ[setting]
    except KeyError:
        error_msg = "Set the %s env variable" % setting
        raise ImproperlyConfigured(error_msg)

os.environ.setdefault(
    'DJANGO_SETTINGS_MODULE', env(
        'DJANGO_SETTINGS_MODULE', default='config.settings.local'
        )
    )

app = Celery('isppme')

# Using a string here means the worker don't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks(lambda: [n.name for n in apps.get_app_configs()])

class CeleryAppConfig(AppConfig):
    name = "isppme.taskapp"
    verbose_name = "Celery Config"

    def ready(self):
        installed_apps = [app_config.name for app_config in apps.get_app_configs()]
        app.autodiscover_tasks(lambda: installed_apps, force=True)

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
