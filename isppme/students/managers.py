import datetime
import random
import string

from django.db import transaction, IntegrityError, models

def gen_isppme_reg_number():
    """
    Generates a random reg number for students
    :return: str
    """

    from .models import Student

    prefix = "R"

    def gen():
        # Create Reg Number (9 Characters)
        # -------------------------------------------
        # Extract Year (in 2 digit format e.g 19 for 2019)
        year = datetime.datetime.today().strftime("%y")
        # Generate 4 random digits from 1000 to 9999
        rand_digits = random.randint(1000, 9999)
        # Create random letter
        rand_letter = random.choice(string.ascii_uppercase)
        return f"{prefix}{str(year)}{str(rand_digits)}{str(rand_letter)}"

    reg_number = gen()
    while Student.objects.filter(reg_number=reg_number).exists():
        # Loop to run and create another reg_number if the assigned
        # reg_number exists already.
        reg_number = gen()
    return reg_number


class StudentManager(models.Manager):
    """Custom student model manager."""

    use_in_migrations = True

    def create_from_application(self, application):
        from isppme.enrollments.models import Enrollment

        try:
            with transaction.atomic():
                student = self.model(
                    name=f"{application.first_name} {application.last_name}",
                    dob=application.date_of_birth,
                    sex=application.sex,
                    place_of_birth=application.place_of_birth,
                    national_id=application.place_of_birth,
                    passport_number=application.passport_number,
                    nationality=application.nationality,
                    country_of_permanent_residence=application.country_of_permanent_residence,
                    disabilities=application.disabilities,
                    contact_address=application.contact_address,
                    telephone_number=application.telephone_number,
                    mobile_number=application.mobile_number,
                    email_address=application.email_address,
                    is_member=application.is_member,
                    member_id=application.member_id,
                    application_ref=application,
                )
                student.save(using=self._db)
                enrollment = Enrollment(
                    student=student,
                    course=application.course,
                )
                enrollment.save()
        except IntegrityError:
            return None # TODO: log exception
        else:
            return student
