from django.apps import AppConfig


class ModulesConfig(AppConfig):
    name = 'isppme.modules'
