from django import forms
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Fieldset, Div, HTML, ButtonHolder, Submit
#
from isppme.semesters.models import Semester
from isppme.exams.models import Exam


class StaffAuthenticationForm(AuthenticationForm):
    def confirm_login_allowed(self, user):
        if not user.is_active:
            raise forms.ValidationError(
                _("This account is inactive."),
                code='inactive',
            )
        if not user.is_staff:
            raise forms.ValidationError(
                _("Account not allowed here."), code="not_allowed"
            )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-prepended'


class AcceptanceForm(forms.Form):
    """
    Form to accept an application
    * Requires that a semester/intake
    be specified upon acceptance.
    """

    application = forms.CharField(
        widget=forms.HiddenInput(), required=True,
    )
    semester = forms.ModelChoiceField(
        queryset=Semester.objects.filter(
            applications_start__lte=timezone.now().date(),
            end__gte=timezone.now().date()
        ).order_by("start"), required=False,
        empty_label="No semester/intake eligible."
    )



class CustomPasswordChangeForm(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Submit'))


class ExamForm(forms.ModelForm):
    class Meta:
        model = Exam
        fields = [
            "module",
            "semester",
            "write_date",
            "write_time",
            "points",
        ]
        widgets = {
            "module": forms.HiddenInput(),
            "semester": forms.HiddenInput(),
            "write_time": forms.TimeInput(
                attrs={
                    "data-toggle": "flatpickr",
                    "data-flatpickr-enable-time": "true",
                    "data-flatpickr-no-calendar": "true",
                    "data-flatpickr-alt-format": "H:i",
                    "data-flatpickr-date-format": "H:i"
                }
            ),
            "write_date": forms.DateInput(
                attrs={"data-toggle": "flatpickr"}
            )
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Submit'))
