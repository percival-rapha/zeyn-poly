from django.contrib import admin


# Customize Header
admin.site.site_header = "Bulawayo Polytechnic Administration Panel"

# Customize Title
admin.site.site_title = "Bulawayo Polytechnic Administration"

# Index Title
admin.site.index_title = "Bulawayo Polytechnic System Administration"
