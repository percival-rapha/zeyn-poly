from django.apps import AppConfig


class ApplicationsConfig(AppConfig):
    name = 'isppme.applications'
