####################################################################
# Course Application Views                                         #
# Written by Percival Rapha (c) ISPPME 2019                        #
####################################################################
import logging

from django.shortcuts import render, get_object_or_404
from django.db import transaction, IntegrityError
from django.views import generic, View
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseForbidden, HttpResponseServerError
from django.db import transaction
from django.utils.decorators import method_decorator

from django_hosts.resolvers import reverse as hosts_reverse
from django_filters.views import FilterView

from isppme.courses.models import Course, CourseName
from isppme.students.models import Student
from isppme.semesters.models import Semester

from .models import Application
from .forms import ApplicationForm, DocumentForm, StudentCreationForm
from .tasks import send_email
from .filters import CourseFilter

logger = logging.getLogger(__name__)

class Index(FilterView):
    template_name = "applications/index.html"
    filterset_class = CourseFilter
    context_object_name = "course_names"
    paginate_by = 10


index_view = Index.as_view()


class ApplicantDetails(generic.CreateView):
    template_name = "applications/application_form.html"
    form_class = ApplicationForm

    def __init__(self, **kwargs):
        self.course = None
        super().__init__(**kwargs)

    def dispatch(self, request, *args, **kwargs):
        APPLICATION_SEMESTER = Semester.objects.get_application_semester()
        if not APPLICATION_SEMESTER:
            return HttpResponseForbidden("Applications not open at the moment.")
        self.course = get_object_or_404(Course, uuid_id=kwargs['course_uuid'])
        self.initial.update(
            {"course": self.course, }
        )
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["course"] = self.course
        return ctx

    def get_success_url(self):
        return hosts_reverse('upload_application_documents', kwargs={'application_uuid': self.object.uuid_id},
                             host='applications', )


applicant_details_view = ApplicantDetails.as_view()


class UploadApplicationDocuments(generic.CreateView):
    template_name = "applications/course_application_document_upload.html"
    form_class = DocumentForm

    def __init__(self, **kwargs):
        self.application = None
        super().__init__(**kwargs)

    def get_success_url(self):
        return hosts_reverse('upload_application_documents', kwargs={'application_uuid': self.application.uuid_id},
                             host='applications', )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["application"] = self.application
        return context

    def dispatch(self, request, *args, **kwargs):
        self.application = get_object_or_404(Application, uuid_id=self.kwargs["application_uuid"])
        self.initial.update({"application": self.application})
        return super().dispatch(request, *args, **kwargs)


upload_application_documents_view = UploadApplicationDocuments.as_view()


class CompleteAplication(View):
    success_template = "applications/application_complete.html"

    def __init__(self, **kwargs):
        self.application = None
        super().__init__(**kwargs)

    def dispatch(self, request, *args, **kwargs):
        self.application = get_object_or_404(Application, uuid_id=kwargs["application_uuid"])
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, **kwargs):
        if self.application.documents.count() < 1:
            # TODO: Add message to indicate why user has been
            # redirected.
            return hosts_reverse('upload_application_documents', kwargs={'application_uuid': self.application.uuid_id},
                                 host='applications', )

        # Check if this process has already been run.
        if self.application.completed:
            return HttpResponseForbidden()
        else:
            # Mark Application as complete:
            self.application.complete()

            # Do post application process tasks
            # Send mail, show success message.
            send_email.delay(self.application.id)

            return HttpResponse(
                render(
                    request, self.success_template, {"application": self.application}
                )
            )


complete_application_view = CompleteAplication.as_view()


class AcknowledgeApplication(View):
    template_name = "applications/acknowledge.html"

    def __init__(self, **kwargs):
        self.application = None
        super().__init__(**kwargs)

    def get(self, request, **kwargs):
        return HttpResponse(
            render(request, self.template_name, {"application": self.application})
        )

    def dispatch(self, request, *args, **kwargs):
        self.application = get_object_or_404(Application, uuid_id=kwargs['application_uuid'])
        # Check if application is accepted
        if not self.application.accepted or self.application.acknowledged:
            return HttpResponseForbidden()  # TODO: Add template with descriptive message
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if request.POST.get("acknowledge", None):
            self.application.acknowledge()
            return HttpResponseRedirect(
                redirect_to=hosts_reverse("create_student_account",
                                          kwargs={"application_uuid": self.application.uuid_id}, host="applications")
            )
        else:
            return HttpResponseForbidden()


acknowledge_application_view = AcknowledgeApplication.as_view()


class CreateStudentAccount(generic.CreateView):
    template_name = "applications/create_student_account.html"
    form_class = StudentCreationForm

    def __init__(self, **kwargs):
        self.application = None
        self.student = None
        super().__init__(**kwargs)

    @method_decorator(transaction.non_atomic_requests)
    def dispatch(self, request, *args, **kwargs):
        self.application = get_object_or_404(Application, uuid_id=kwargs['application_uuid'])
        if not self.application.accepted:
            return HttpResponseForbidden("Application not accepted.")

        if hasattr(self.application, "student"):
            self.student = self.application.student
        else:
            self.student = Student.objects.create_from_application(self.application)

        self.initial.update(
            {
                "username": self.student.reg_number,
                "email": self.student.email_address,
            }
        )
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["application"] = self.application
        return context

    @method_decorator(transaction.non_atomic_requests)
    def form_valid(self, form):
        new_user = form.save(commit=True)
        # print(new_user)
        self.student.set_user(new_user)
        if not self.student.application_ref:
            try:
                self.student.application_ref = self.application
            except Exception as ex:
                print(str(ex))
        self.student.save()
        if self.student.user:
            # print(self.student.user)
            return HttpResponse(
                render(self.request, "applications/student_created.html", {"student": self.student})
            )
        else:
            return HttpResponseServerError("There was an error creating your account. Please try again later.")


create_student_account_view = CreateStudentAccount.as_view()
